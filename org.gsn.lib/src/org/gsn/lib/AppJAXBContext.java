package org.gsn.lib;

import java.io.*;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.*;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;

import static com.google.common.collect.Lists.newArrayList;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.eclipse.core.resources.IResource.DEPTH_INFINITE;
import static org.eclipse.core.runtime.IStatus.ERROR;

public class AppJAXBContext {
	public static final String BUNDLE_ID = "org.gsn.lib";

	public static CoreException toCoreException(final Exception exception, final String bundleID) {
		return toCoreException(exception, bundleID, "");
	}

	public static CoreException toCoreException(final Exception exception, final String bundleID, final String message) {
		if (exception instanceof CoreException)
			return (CoreException) exception;
		return new CoreException(new Status(ERROR, bundleID, message + " " + exception.getMessage()));
	}

	public static CoreException toCoreException(final String bundleID, final String message) {
		return new CoreException(new Status(ERROR, bundleID, message));
	}

	/**
	 * @param object object's class must have the @XmlRootElement annotation
	 * @return
	 */
	public static <T> T copyObject(final T object) {
		if (object == null)
			return null;
		try {
			return new AppJAXBContext(BUNDLE_ID, object.getClass()).copy(object);
		} catch (final CoreException e) {
			throw new RuntimeException(e);
		}
	}

	private final String bundleID;
	private final JAXBContext context;
	private final Marshaller marshaller;
	private final Unmarshaller unmarshaller;
	private final Schema schema;

	public AppJAXBContext(final String bundleID, final Class<?>... clases) throws CoreException {
		this(bundleID, false, clases);
	}

	public AppJAXBContext(final String bundleID, final boolean validate, final Class<?>... clases) throws CoreException {
		this.bundleID = bundleID;
		try {
			context = JAXBContext.newInstance(clases);
			schema = validate ? genSchema() : null;
			marshaller = context.createMarshaller();
			marshaller.setSchema(schema);
			unmarshaller = context.createUnmarshaller();
			unmarshaller.setSchema(schema);
			marshaller.setProperty(JAXB_FORMATTED_OUTPUT, true);
			unmarshaller.setEventHandler(new ValidationEventHandler() {

				@Override
				public boolean handleEvent(final ValidationEvent arg0) {
					System.out.println(arg0.getMessage());
					return true;
				}
			});
		} catch (final JAXBException exception) {
			throw toCoreException(exception, bundleID, "Error could not create JAXBContext");
		}
	}

	public Marshaller marshaller() {
		return marshaller;
	}

	public Unmarshaller unmarshaller() {
		return unmarshaller;
	}

	public JAXBContext context() {
		return context;
	}

	private Schema genSchema() throws CoreException {
		try {
			final List<ByteArrayOutputStream> outs = newArrayList();
			synchronized (context) {
				context.generateSchema(new SchemaOutputResolver() {
					@Override
					public Result createOutput(final String namespaceUri, final String suggestedFileName) throws IOException {
						final ByteArrayOutputStream out = new ByteArrayOutputStream();
						outs.add(out);
						final StreamResult streamResult = new StreamResult(out);
						streamResult.setSystemId("");
						return streamResult;
					}
				});
			}
			final List<StreamSource> sources = newArrayList();
			for (final ByteArrayOutputStream out : outs) {
				// to examine schema: System.out.append(new String(out.toByteArray()));
				sources.add(new StreamSource(new ByteArrayInputStream(out.toByteArray()), ""));
			}
			Collections.reverse(sources); // I am really not sure why this works, but it is the only way to generate the
											// Application JAXB schema on the fly.
			return SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI).newSchema(sources.toArray(new StreamSource[0]));
		} catch (final Exception e) {
			throw toCoreException(e, bundleID, "Could not generate schema for validation.");
		}
	}

	public InputStream toInputStream(final Object object) throws CoreException {
		try {
			final StringWriter writer = new StringWriter();
			synchronized (context) {
				marshaller().marshal(object, writer);
			}
			return new ByteArrayInputStream(writer.toString().getBytes());
		} catch (final JAXBException e) {
			throw toCoreException(e, bundleID, "Error could not marshall model object " + object);
		}
	}

	public synchronized AppJAXBContext save(final Object object, final IFile file, final IProgressMonitor monitor)
			throws CoreException {
		if (!file.exists())
			file.create(toInputStream(object), true, monitor);
		else
			file.setContents(toInputStream(object), true, true, monitor);
		file.refreshLocal(DEPTH_INFINITE, monitor);
		return this;
	}

	public synchronized AppJAXBContext save(final Object object, final File file) throws CoreException {
		try {
			copyInputStreamToFile(toInputStream(object), file);
		} catch (final IOException e) {
			throw toCoreException(e, bundleID, "Error could not save JAXB object to file: " + file);
		}
		return this;
	}

	public synchronized Object load(final IFile file) throws CoreException {
		return load(file, null, bundleID);
	}

	public synchronized Object load(final IFile file, final IProgressMonitor monitor) throws CoreException {
		return load(file, monitor, bundleID);
	}

	public synchronized Object load(final IFile file, final IProgressMonitor monitor, final String bundleID) throws CoreException {
		final InputStream input = file.getContents(true);
		try {
			synchronized (context) {
				final Object object = unmarshaller().unmarshal(input);
				return object;
			}
		} catch (final JAXBException e) {
			throw toCoreException(e, bundleID, "Error could not unmarshall JAXB object from file " + file);
		} finally {
			closeQuietly(input);
		}
	}

	public synchronized Object load(final String input, final IProgressMonitor monitor) throws CoreException {
		final StringReader reader = new StringReader(input);
		try {
			synchronized (context) {
				final Object object = unmarshaller().unmarshal(reader);
				return object;
			}
		} catch (final JAXBException e) {
			throw toCoreException(e, bundleID, "Error could not unmarshall JAXB object from string input: " + input.toString());
		} finally {
			reader.close();
		}
	}

	public synchronized Object load(final File file) throws CoreException {
		return load(file, null, bundleID);
	}

	public synchronized Object load(final File file, final IProgressMonitor monitor) throws CoreException {
		return load(file, monitor, bundleID);
	}

	public synchronized Object load(final File file, final IProgressMonitor monitor, final String bundleID) throws CoreException {
		try {
			synchronized (context) {
				final Object object = unmarshaller().unmarshal(file);
				return object;
			}
		} catch (final JAXBException e) {
			throw toCoreException(e, bundleID, "Error could not unmarshall JAXB object from file " + file);
		}
	}

	/**
	 * ** Use with Care ** Beware of using this method as a clone method, deserializing an object into an XML String and then
	 * reading it back in has performance issues.
	 * 
	 * @param object object's class must have the @XmlRootElement annotation
	 * @return
	 * @throws CoreException
	 */
	@SuppressWarnings("unchecked")
	public <T> T copy(final T object) {
		if (object == null)
			return null;
		final StringWriter writer = new StringWriter();
		try {
			synchronized (context) {
				marshaller().marshal(object, writer);
				final StringReader reader = new StringReader(writer.toString());
				return (T) unmarshaller().unmarshal(reader);
			}
		} catch (final JAXBException e) {
			throw new RuntimeException(toCoreException(e, bundleID, "Error could not copy object: " + object));
		}
	}
}