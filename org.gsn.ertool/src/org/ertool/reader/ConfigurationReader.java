/*******************************************************************************
 * Copyright (c) 2013, Neil Walkinshaw
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Leicester nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NEIL WALKINSHAW BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.ertool.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

import net.sourceforge.olduvai.treejuxtaposer.TreeParser;
import net.sourceforge.olduvai.treejuxtaposer.drawer.Tree;
import net.sourceforge.olduvai.treejuxtaposer.drawer.TreeNode;

public class ConfigurationReader {

	 public static Tree readTree(String treeFile) throws FileNotFoundException {
	 	File f = new File(treeFile);
        BufferedReader r = new BufferedReader(new FileReader(f));
        TreeParser tp = new TreeParser(r);
        Tree tree = tp.tokenize(1, "tree", null);
        return tree;
    }
	 
	 public static HashMap<TreeNode,Double[]>  readBeliefs(Tree t, String treeFile) throws FileNotFoundException {
		 	
		 HashMap<TreeNode,Double[]> beliefMap = new HashMap<TreeNode,Double[]>();
		 File f = new File(treeFile);
	     BufferedReader r = new BufferedReader(new FileReader(f));
	     try {
			while(!r.readLine().startsWith("#leaf_beliefs"))
				 continue;
			String line = r.readLine().trim();
			int scale = Integer.valueOf(line);
			while((line = r.readLine())!=null){
				if(line.startsWith("#"))
					continue;
				String[] tokens = line.split(" ");
				TreeNode node = t.getNodeByName(tokens[0]);
				Double[] distribution = new Double[tokens.length-1];
				for(int i = 1; i<tokens.length;i++)
					distribution[i-1] = Double.valueOf(tokens[i]);
				beliefMap.put(node, distribution);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return beliefMap;
	     
	    }
	
}
