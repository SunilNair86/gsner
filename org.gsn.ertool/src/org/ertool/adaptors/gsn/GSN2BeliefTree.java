package org.ertool.adaptors.gsn;

import org.ertool.model.BeliefDAG;
import org.gsn.model.business.AbstractNode;

public class GSN2BeliefTree {

	protected AbstractNode root;

	public GSN2BeliefTree(final AbstractNode root) {
		this.root = root;
	}

	public AbstractNode findNode(final String name) {
		return search(root, name);
	}

	private AbstractNode search(final AbstractNode node, final String name) {
		if (node.getName().equals(name)) {
			return node;
		}

		if (!node.isSolution()) { // must have children
			final AbstractNode n = node;
			for (final AbstractNode child : n.getChildren().keySet()) {
				final AbstractNode found = search(child, name);
				if (found != null)
					return found;
			}
		}
		return null;
	}

	public BeliefDAG getBeliefDAG() {
		return new BeliefDAG(root);
	}

}
