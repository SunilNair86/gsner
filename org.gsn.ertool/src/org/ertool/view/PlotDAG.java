/*******************************************************************************
 * Copyright (c) 2013, Neil Walkinshaw
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Leicester nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NEIL WALKINSHAW BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.ertool.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.ertool.model.BeliefDAG;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.QuestionResponse;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

public class PlotDAG {

	private final BeliefDAG tree;
	private final Map<AbstractNode, JFreeChart> nodeMap;
	private final IPath filePath;

	public PlotDAG(final BeliefDAG tree, final IPath filePath) {
		this.tree = tree;
		this.filePath = filePath;
		nodeMap = new HashMap<AbstractNode, JFreeChart>();
		createCharts();
		createDotTree();
	}

	private void createDotTree() {
		try {
			final FileWriter outFile = new FileWriter("finalTree.dot");
			final PrintWriter out = new PrintWriter(outFile);

			out.println("digraph{");
			final Iterator<AbstractNode> nodeIt = nodeMap.keySet().iterator();
			while (nodeIt.hasNext()) {
				final AbstractNode current = nodeIt.next();
				if (current instanceof QuestionResponse)
					continue;
				out.println(current.getName() + " " + "[shape=box style=filled fillcolor=palegreen label=< <table border=\"0\">"
						+ " <tr> <td border=\"0\"><img src=\"" + "tmp" + File.separator + current.getName() + ".png"
						+ "\"/></td> </tr>  </table> >]");
				out.println();
			}

			final AbstractNode structure = tree.getDAG();
			final Collection<AbstractNode> nodes = new HashSet<AbstractNode>();
			getNodes(structure, nodes);
			final Iterator<AbstractNode> treeIt = nodes.iterator();
			while (treeIt.hasNext()) {
				final AbstractNode n = treeIt.next();
				if (n.isSolution())
					continue;
				final AbstractNode node = n;
				final Set<AbstractNode> children = new HashSet<AbstractNode>();
				for (final AbstractNode child : node.getChildren().keySet()) {
					if (!(child instanceof QuestionResponse))
						children.add(child);
				}
				for (final AbstractNode child : children) {
					out.println(n.getName() + "->" + child.getName() + "[dir=\"none\"," + "style=\"setlinewidth(" + 4
							* node.getWeight(child) + ")\",label=\"" + node.getWeight(child) + "\", fontsize=22]");
				}
			}
			out.println("}");
			out.close();
		} catch (final IOException e) {
			e.printStackTrace();
		}

	}

	public static void getNodes(final AbstractNode root, final Collection<AbstractNode> current) {
		current.add(root);
		if (root instanceof AbstractNode) {
			final AbstractNode n = root;
			final Set<AbstractNode> children = new HashSet<AbstractNode>();
			for (final AbstractNode child : n.getChildren().keySet()) {
				if (!(child instanceof QuestionResponse))
					children.add(child);
			}
			for (final AbstractNode an : children) {
				getNodes(an, current);
			}

		}
	}

	/**
	 * A custom renderer that returns a different color for each item in a single series.
	 */
	class CustomRenderer extends BarRenderer {

		/** The colors. */
		private final Paint[] colors;

		/**
		 * Creates a new renderer.
		 * 
		 * @param colors the colors.
		 */
		public CustomRenderer(final Paint[] colors) {
			this.colors = colors;
		}

		/**
		 * Returns the paint for an item. Overrides the default behaviour inherited from AbstractSeriesRenderer.
		 * 
		 * @param row the series.
		 * @param column the category.
		 * 
		 * @return The item color.
		 */
		@Override
		public Paint getItemPaint(final int row, final int column) {
			return this.colors[column % this.colors.length];
		}
	}

	private void createCharts() {
		final Map<AbstractNode, Double[]> beliefs = tree.getBeliefs();
		final Iterator<AbstractNode> nodeIt = beliefs.keySet().iterator();
		while (nodeIt.hasNext()) {
			final AbstractNode node = nodeIt.next();
			final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
			final Double[] distribution = beliefs.get(node);
			final Paint[] painter = new Paint[tree.getScale() + 1];
			for (int i = 0; i < tree.getScale(); i++) {
				painter[i] = Color.GREEN;
			}
			painter[tree.getScale()] = Color.RED;
			final CategoryItemRenderer renderer = new CustomRenderer(painter);
			int lim = tree.getScale();
			if (node.equals(tree.getDAG()))
				lim++;

			for (int i = 0; i < lim; i++) {
				String label = Integer.toString(i + 1);
				if (i > (tree.getScale() - 1))
					label = "?";
				dataset.addValue(distribution[i], node.getName(), label);
			}

			final JFreeChart jfc = ChartFactory.createBarChart(node.getName(), "Perceived Quality", "Confidence", dataset,
					PlotOrientation.VERTICAL, false, false, false);
			final CategoryPlot p = (CategoryPlot) jfc.getPlot();
			p.setRenderer(renderer);
			final ValueAxis range = p.getRangeAxis();
			range.setRange(0.00, 1.00);
			range.setVerticalTickLabels(true);

			final Font f = new Font("sansserif", Font.BOLD, 18);
			range.setTickLabelFont(f);
			range.setLabelFont(f);
			p.getDomainAxis().setLabelFont(f);
			p.getDomainAxis().setTickLabelFont(f);
			jfc.setAntiAlias(true);

			nodeMap.put(node, jfc);
			try {
				ChartUtilities.saveChartAsPNG(filePath.toFile(), jfc, 400, 300);
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
