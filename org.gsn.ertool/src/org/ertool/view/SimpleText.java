/*******************************************************************************
 * Copyright (c) 2013, Neil Walkinshaw
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Leicester nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NEIL WALKINSHAW BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.ertool.view;

import java.util.Iterator;
import java.util.Map;

import org.ertool.model.BeliefTree;

import net.sourceforge.olduvai.treejuxtaposer.drawer.TreeNode;

public class SimpleText {
	
	public static String toString(BeliefTree bt){
		Map<TreeNode,Double[]> beliefs = bt.getBeliefs();
		int scale = bt.getScale();
		String s = new String();
		Iterator<TreeNode> nodeIt = beliefs.keySet().iterator();
		while(nodeIt.hasNext()){
			TreeNode name = nodeIt.next();
			s = s + name.getName()+": ";
			Double[] distribution = beliefs.get(name);
			for (int i = 0; i< distribution.length;i++) {
				if(i == scale)
					s = s + ", [unknown:]"+distribution[i];
				else
					s = s + ", ["+(i+1)+"]"+distribution[i];
			}
			s = s + "\n";
		}
		return s;
	}

}
