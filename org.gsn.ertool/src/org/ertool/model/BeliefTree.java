/*******************************************************************************
 * Copyright (c) 2013, Neil Walkinshaw
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Leicester nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NEIL WALKINSHAW BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.ertool.model;

import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.ertool.app.ERTool;

import net.sourceforge.olduvai.treejuxtaposer.drawer.Tree;
import net.sourceforge.olduvai.treejuxtaposer.drawer.TreeNode;

public class BeliefTree extends BeliefStructure {
	
	private final static Logger LOGGER = Logger.getLogger(BeliefTree.class.getName()); 
	
	protected Tree baseTree;
	
	// Belief values for the leaf-nodes
	protected Map<TreeNode,Double[]> beliefs = new HashMap<TreeNode,Double[]>();
	
	
	//Beliefs should contain the initial belief distributions for the leaf nodes
	//Assumes that the length of all belief distributions is the same as scale
	public BeliefTree(Tree baseTree, Map<TreeNode,Double[]> beliefs, int scale){
		this.baseTree = baseTree; 
		this.scale = scale;
		if(!ERTool.DEBUG)
			LOGGER.setLevel(Level.OFF);
		dfsPopulate(beliefs);
		
	}
	
	//Beliefs should contain the initial belief distributions for the leaf nodes
	//Assumes that the length of all belief distributions is the same as scale
	public BeliefTree(Tree baseTree, Map<TreeNode,Double[]> beliefs){
		this.baseTree = baseTree; 
		if(!ERTool.DEBUG)
			LOGGER.setLevel(Level.OFF);
		dfsPopulate(beliefs);
	}
	
	protected void dfsPopulate(Map<TreeNode,Double[]> initBeliefs){
		computeBeliefDistribution(baseTree.getRoot(),initBeliefs);
	}

	protected Double[] computeBeliefDistribution(TreeNode node, Map<TreeNode,Double[]> initBeliefs) {
		if(node.isLeaf()){
			Double[] distribution = initBeliefs.get(node);
			beliefs.put(node, distribution);
			return distribution;
		}
		else{
			TreeNode[] children = new TreeNode[node.numberChildren()];
			for(int i = 0; i< node.numberChildren(); i++)
				children[i]=node.getChild(i);
			Double[] distribution = combineBeliefs(children, initBeliefs);
			beliefs.put(node, distribution);
			return distribution;
		}
		
	}

	protected Double[] combineBeliefs(TreeNode[] children, Map<TreeNode,Double[]> initBeliefs) {
		Map<TreeNode,Double[]> beliefTable = computeBeliefTable(children, initBeliefs);
		Map<TreeNode,Double[]> beliefMassTable = computeBeliefMassTable(beliefTable, children);
		Double[] aggregateMasses = aggregate(beliefMassTable);
		return transformToBeliefs(aggregateMasses);
	}



	

	private Double[] aggregate(Map<TreeNode, Double[]> beliefMassTable) {
		Iterator<TreeNode> childIterator = beliefMassTable.keySet().iterator();
		TreeNode current = childIterator.next();
		Double[] currentMasses = beliefMassTable.get(current);
		while(childIterator.hasNext()){
			TreeNode next = childIterator.next();
			Double[] nextMasses = beliefMassTable.get(next);
			logPreCombination(current,currentMasses,next,nextMasses);
			Double[] combinedMasses = computeCombination(currentMasses,
					nextMasses);
			logCombined(combinedMasses);
			currentMasses = combinedMasses;
		}
		return currentMasses;
	}


	private void logPreCombination(TreeNode current, Double[] currentMasses, TreeNode next,
			Double[] nextMasses) {
		String logString = "Combining "+current.getName()+" with "+next.getName()+"\n";
		logString = logString + current.getName() +":\n"+getMassesString(currentMasses)+
				"\n"+next.getName()+"\n"+getMassesString(nextMasses);
		LOGGER.info(logString);
		
	}
	
	private Map<TreeNode,Double[]> computeBeliefMassTable(Map<TreeNode,Double[]> beliefTable, TreeNode[] children) {
		Map<TreeNode,Double[]> massTable = new HashMap<TreeNode,Double[]>();
		double weightTotal = 0D;
		for(int i = 0; i<children.length;i++){
			weightTotal+=children[i].getWeight();
		}
		for(int i = 0; i<children.length;i++){
			TreeNode child = children[i];
			double weight = child.weight/weightTotal;
			Double[] beliefs = beliefTable.get(child);
			Double[] mass = new Double[scale+3];
			for(int j = 0;j<scale;j++){
				mass[j] = (double)beliefs[j]*weight;
			}
			mass[scale] = weight*(1-sum(beliefs)); //mtilde
			mass[scale+1] = 1-weight; //mbar
			mass[scale+2] = mass[scale] + mass[scale+1];
			massTable.put(child, mass);
			//this.beliefs.put(child, mass);
		}
		return massTable;
	}

	

	private Map<TreeNode,Double[]> computeBeliefTable(TreeNode[] children, Map<TreeNode,Double[]> initBeliefs) {
		HashMap<TreeNode,Double[]> beliefTable = new HashMap<TreeNode,Double[]>();
		for(int i = 0; i<children.length;i++){
			TreeNode child = children[i];
			Double[] distribution = computeBeliefDistribution(child, initBeliefs);
			beliefTable.put(child, distribution);
		}
		return beliefTable;
	}

	
	public Map<TreeNode,Double[]> getBeliefs(){
		return beliefs;
	}
	
	public Tree getTree(){
		return baseTree;
	}
	
	
}
