/*******************************************************************************
 * Copyright (c) 2013, Neil Walkinshaw
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Leicester nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL NEIL WALKINSHAW BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.ertool.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ertool.app.ERTool;
import org.gsn.model.business.AbstractNode;

public class BeliefDAG extends BeliefStructure {

	private final static Logger LOGGER = Logger.getLogger(BeliefDAG.class.getName());

	protected AbstractNode baseTree;

	// Belief values for the leaf-nodes
	protected Map<AbstractNode, Double[]> beliefs = new HashMap<AbstractNode, Double[]>();

	// Beliefs should contain the initial belief distributions for the leaf nodes
	// Assumes that the length of all belief distributions is the same as scale
	public BeliefDAG(final AbstractNode baseTree) {
		this.baseTree = baseTree;
		populateBeliefs(baseTree);
		if (!ERTool.DEBUG)
			LOGGER.setLevel(Level.OFF);
		dfsPopulate(beliefs);

	}

	public BeliefDAG(final AbstractNode baseTree, final int scale) {
		this.baseTree = baseTree;
		this.scale = scale;
		populateBeliefs(baseTree);
		if (!ERTool.DEBUG)
			LOGGER.setLevel(Level.OFF);
		dfsPopulate(beliefs);
	}

	private void populateBeliefs(final AbstractNode node) {
		if (node.isSolution()) {
			beliefs.put(node, node.getDistribution());
		} else {
			final AbstractNode n = node;
			for (final AbstractNode child : n.getChildren().keySet()) {
				populateBeliefs(child);
			}
		}
	}

	// Beliefs should contain the initial belief distributions for the leaf nodes
	// Assumes that the length of all belief distributions is the same as scale
	public BeliefDAG(final AbstractNode baseTree, final Map<AbstractNode, Double[]> beliefs, final int scale) {
		this.baseTree = baseTree;
		this.scale = scale;
		if (!ERTool.DEBUG)
			LOGGER.setLevel(Level.OFF);
		dfsPopulate(beliefs);

	}

	// Beliefs should contain the initial belief distributions for the leaf nodes
	// Assumes that the length of all belief distributions is the same as scale
	public BeliefDAG(final AbstractNode baseTree, final Map<AbstractNode, Double[]> beliefs) {
		this.baseTree = baseTree;
		if (!ERTool.DEBUG)
			LOGGER.setLevel(Level.OFF);
		dfsPopulate(beliefs);
	}

	protected void dfsPopulate(final Map<AbstractNode, Double[]> initBeliefs) {
		computeBeliefDistribution(baseTree, initBeliefs);
	}

	protected Double[] computeBeliefDistribution(final AbstractNode node, final Map<AbstractNode, Double[]> initBeliefs) {
		if (node.isSolution()) {
			final Double[] distribution = initBeliefs.get(node);
			beliefs.put(node, distribution);
			return distribution;
		} else {
			final AbstractNode n = node;
			final Map<AbstractNode, Double> children = n.getChildren();
			final Double[] distribution = combineBeliefs(n, children.keySet().toArray(new AbstractNode[children.size()]),
					initBeliefs);
			beliefs.put(node, distribution);
			return distribution;
		}

	}

	protected Double[] combineBeliefs(final AbstractNode parent, final AbstractNode[] children,
			final Map<AbstractNode, Double[]> initBeliefs) {
		final Map<AbstractNode, Double[]> beliefTable = computeBeliefTable(children, initBeliefs);
		final Map<AbstractNode, Double[]> beliefMassTable = computeBeliefMassTable(parent, beliefTable, children);
		final Double[] aggregateMasses = aggregate(beliefMassTable);
		return transformToBeliefs(aggregateMasses);
	}

	private Double[] aggregate(final Map<AbstractNode, Double[]> beliefMassTable) {
		final Iterator<AbstractNode> childIterator = beliefMassTable.keySet().iterator();
		final AbstractNode current = childIterator.next();
		Double[] currentMasses = beliefMassTable.get(current);
		while (childIterator.hasNext()) {
			final AbstractNode next = childIterator.next();
			final Double[] nextMasses = beliefMassTable.get(next);
			logPreCombination(current, currentMasses, next, nextMasses);
			final Double[] combinedMasses = computeCombination(currentMasses, nextMasses);
			logCombined(combinedMasses);
			currentMasses = combinedMasses;
		}
		return currentMasses;
	}

	private void logPreCombination(final AbstractNode current, final Double[] currentMasses, final AbstractNode next,
			final Double[] nextMasses) {
		String logString = "Combining " + current.getName() + " with " + next.getName() + "\n";
		logString = logString + current.getName() + ":\n" + getMassesString(currentMasses) + "\n" + next.getName() + "\n"
				+ getMassesString(nextMasses);
		LOGGER.info(logString);

	}

	private Map<AbstractNode, Double[]> computeBeliefMassTable(final AbstractNode parent,
			final Map<AbstractNode, Double[]> beliefTable, final AbstractNode[] children) {
		final Map<AbstractNode, Double[]> massTable = new HashMap<AbstractNode, Double[]>();
		double weightTotal = 0D;
		for (int i = 0; i < children.length; i++) {
			weightTotal += parent.getWeight(children[i]);
		}
		for (int i = 0; i < children.length; i++) {
			final AbstractNode child = children[i];
			final double weight = parent.getWeight(children[i]) / weightTotal;
			final Double[] beliefs = beliefTable.get(child);
			final Double[] mass = new Double[scale + 3];
			for (int j = 0; j < scale; j++) {
				mass[j] = beliefs[j] * weight;
			}
			mass[scale] = weight * (1 - sum(beliefs)); // mtilde
			mass[scale + 1] = 1 - weight; // mbar
			mass[scale + 2] = mass[scale] + mass[scale + 1];
			massTable.put(child, mass);
			// this.beliefs.put(child, mass);
		}
		return massTable;
	}

	private Map<AbstractNode, Double[]> computeBeliefTable(final AbstractNode[] children,
			final Map<AbstractNode, Double[]> initBeliefs) {
		final HashMap<AbstractNode, Double[]> beliefTable = new HashMap<AbstractNode, Double[]>();
		for (int i = 0; i < children.length; i++) {
			final AbstractNode child = children[i];
			final Double[] distribution = computeBeliefDistribution(child, initBeliefs);
			beliefTable.put(child, distribution);
		}
		return beliefTable;
	}

	public Map<AbstractNode, Double[]> getBeliefs() {
		return beliefs;
	}

	public AbstractNode getDAG() {
		return baseTree;
	}

}
