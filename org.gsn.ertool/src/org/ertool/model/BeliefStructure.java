package org.ertool.model;

import java.text.DecimalFormat;
import java.util.logging.Logger;

public abstract class BeliefStructure {
	

	
	private final static Logger LOGGER = Logger.getLogger(BeliefStructure.class.getName()); 
	
		// Number of levels in scale (default of 5 is the likert scale, where 0=awful and 5 = excellent
		protected int scale = 5;
		
		protected Double[] transformToBeliefs(Double[] aggregateMasses) {
			Double[] beliefs = new Double[scale+1];
			for(int i = 0;i<scale;i++){
				beliefs[i] = aggregateMasses[i]/(1-aggregateMasses[scale+1]);
			}
			beliefs[scale]=aggregateMasses[scale]/(1-aggregateMasses[scale+1]); //tilde/1-bar
			logBeliefs(beliefs);
			return beliefs;
		}
		
		protected void logCombined(Double[] combined) {
			String logString = "Combined Masses:\n"+getMassesString(combined);
			LOGGER.info(logString);
		}
		
		protected void logBeliefs(Double[] beliefs){
			String logString = "Beliefs:\n";
			for(int j = 0; j<scale; j++){
				logString = logString + "b"+(j+1)+"="+round(beliefs[j],2)+", ";
			}
			logString+="bH ="+round(beliefs[scale],2)+",";
			LOGGER.info(logString);
		}

		protected String getMassesString(Double[] currentMasses) {
			String retString = "";
			for(int j = 0; j<scale; j++){
				retString = retString + "m"+(j+1)+"="+round(currentMasses[j],2)+", ";
			}
			retString+="m~ ="+round(currentMasses[scale],2)+",";
			retString+="m- ="+round(currentMasses[scale+1],2)+",";
			retString+="mH ="+round(currentMasses[scale+2],2)+"\n";
			return retString;
		}

		protected double computeWeight(Double[] current, Double[] next) {
			double sum = 0D;
			String debugString = "k=1/(1-(";
			boolean first = true;
			for(int i = 0; i<scale; i++){
				for(int j = 0; j<scale; j++){
					
					if(j != i){
						double product = current[i]* next[j];
						sum+=product;
						if(current[i]!=0D && next[j]!=0D){
							if(!first)
								debugString+="+";
							debugString+="("+round(current[i],2)+"*"+round(next[j],2)+")";
							first = false;
						}
					}
				}
			}
			debugString+="))\n";
			double weight = Math.pow(1D-sum, -1D);
			debugString+="="+round(weight,2);
			LOGGER.info(debugString);
			return weight;
		}
		
		
		public static double round(double value, int places) {
			DecimalFormat df = new DecimalFormat("#.###");      
			return Double.valueOf(df.format(value));
		}
		
		/*
		 * Computes the combination of belief functions.
		 */
		public Double[] computeCombination(Double[] currentMasses,
				Double[] nextMasses) {
			double weight = computeWeight(currentMasses,nextMasses);
			Double[] combinedMasses = new Double[scale+3];
			for(int i = 0;i<scale;i++){
				combinedMasses[i] = weight*((currentMasses[i]*nextMasses[i])
						+(currentMasses[scale+2]*nextMasses[i])+(currentMasses[i]*nextMasses[scale+2]));
			}
			combinedMasses[scale] = weight*((currentMasses[scale]*nextMasses[scale])+
					(currentMasses[scale+1]*nextMasses[scale])+
					(currentMasses[scale]*nextMasses[scale+1])); //tilde
			combinedMasses[scale+1] = weight*(currentMasses[scale+1]*nextMasses[scale+1]); //bar
			combinedMasses[scale+2] = combinedMasses[scale] + combinedMasses[scale+1];
			return combinedMasses;
		}
		
		protected double sum(Double[] beliefs) {
			double ret = 0D;
			for (int i = 0; i<scale;i++) {
				ret+=beliefs[i];
			}
			return ret;
		}
		
		public int getScale(){
			return scale;
		}

}
