package org.gsn.model.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class InvalidWeightsException extends Exception {
	private static final long serialVersionUID = -6341219542870564621L;
	private final String message;

	public InvalidWeightsException(final String string) {
		message = string;
	}

}
