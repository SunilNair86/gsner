package org.gsn.model.business;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class QuestionResponse extends Solution implements Serializable {
	private static final long serialVersionUID = 4540067319393614355L;
	private final String question;

	public QuestionResponse(final String question, final String name) {
		super(name);
		this.question = question;
	}

	public QuestionResponse(final String question, final String name, final int score, final double confidence) {
		super(name);
		this.question = question;
		setAnswer(score, confidence);
	}

	public String getQuestion() {
		return question;
	}

	public void setAnswer(final int score, final double confidence) {
		assert (confidence >= 0D);
		assert (confidence <= 1D);
		assert (score >= 0D);
		assert (score <= distLength - 1);

		for (int i = 0; i < distLength; i++) {
			if (i == score)
				distribution[i] = confidence;
			else
				distribution[i] = 0D;
		}
	}

	@Override
	public boolean isSolution() {
		return true;
	}
}
