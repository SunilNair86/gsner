package org.gsn.model.business;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.util.GsnNameUtil;

@Data
@EqualsAndHashCode(callSuper = true)
public class Strategy extends AbstractNode {
	private static final long serialVersionUID = -710309047878364862L;

	@Override
	public Object clone() throws CloneNotSupportedException {
		final Strategy strategy = new Strategy();
		strategy.setColor(this.getRed(), this.getBlue(), this.getGreen());
		strategy.setName(GsnNameUtil.newGenericName((GsnDiagram) getParent(), Strategy.class));
		strategy.setIdentifier(this.getIdentifier());
		strategy.setAttributes(this.getAttributes());
		strategy.setDescription(this.getDescription());
		strategy.setToBeInstantiated(this.isToBeInstantiated());
		strategy.setParent(this.getParent());
		strategy.setLayout(new Rectangle(getLayout().x + 10, getLayout().y + 10, getLayout().width, getLayout().height));

		return strategy;
	}

}
