package org.gsn.model.business;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.util.GsnNameUtil;

import com.google.common.collect.Lists;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Solution extends AbstractNode {
	private static final long serialVersionUID = -9063781741977214562L;

	private List<AbstractNode> responses = Lists.newArrayList();

	public Solution(final String name) {
		super();
		setName(name);
	}

	@Override
	public boolean isSolution() {
		return true;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		final Solution solution = new Solution();
		solution.setName(GsnNameUtil.newGenericName((GsnDiagram) getParent(), Solution.class));
		solution.setIdentifier(this.getIdentifier());
		solution.setAttributes(this.getAttributes());
		solution.setDescription(this.getDescription());
		solution.setToBeInstantiated(this.isToBeInstantiated());
		solution.setParent(this.getParent());
		solution.setColor(this.getRed(), this.getBlue(), this.getGreen());
		solution.setLayout(new Rectangle(getLayout().x + 10, getLayout().y + 10, getLayout().width, getLayout().height));
		return solution;
	}
}
