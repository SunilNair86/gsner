package org.gsn.model.business;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GsnDiagram extends AbstractNode {
	private static final long serialVersionUID = 1798249247068220882L;

	public GsnDiagram() {
		setName("GSN Diagram");
	}
}
