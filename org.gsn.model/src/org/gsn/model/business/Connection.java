package org.gsn.model.business;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.views.properties.IPropertySource;
import org.gsn.model.properties.ConnectionPropertySource;

@Data
@EqualsAndHashCode()
public class Connection implements IAdaptable, Serializable {
	private static final long serialVersionUID = 5866138504684695445L;
	public static final int CONNECTION_DESIGN = 1;
	public static final int CONNECTION_RESOURCES = 2;

	private int connectionType;
	protected AbstractNode sourceNode;
	protected AbstractNode targetNode;
	private GsnDiagram acp = null;
	private IPropertySource propertySource = null;
	private PropertyChangeSupport listeners;

	public static final String PROPERTY_ACP = "ConnectionACP";
	public static final String PROPERTY_ACP_RENAME = "ConnectionACPRename";

	public Connection(final AbstractNode sourceNode, final AbstractNode targetNode, final int connectionType) {
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
		this.connectionType = connectionType;
		this.listeners = new PropertyChangeSupport(this);
	}

	public void connect() {
		sourceNode.addConnections(this);
		targetNode.addConnections(this);
	}

	public void disconnect() {
		sourceNode.removeConnection(this);
		targetNode.removeConnection(this);
	}

	public void reconnect(final AbstractNode sourceNode, final AbstractNode targetNode) {
		if (sourceNode == null || targetNode == null || sourceNode == targetNode) {
			throw new IllegalArgumentException();
		}
		disconnect();
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
		connect();
	}

	public void setAcp(final GsnDiagram acp) {
		this.acp = acp;
		getListeners().firePropertyChange(PROPERTY_ACP, null, acp);
	}

	public void setAcpName(final String name) {
		final String oldName = acp.name;
		acp.name = name;
		getListeners().firePropertyChange(PROPERTY_ACP_RENAME, oldName, this.acp.name);
	}

	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		listeners.addPropertyChangeListener(listener);
	}

	public PropertyChangeSupport getListeners() {
		return listeners;
	}

	public void removePropertyChangeListener(final PropertyChangeListener listener) {
		listeners.removePropertyChangeListener(listener);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(final Class adapter) {
		if (adapter == IPropertySource.class) {
			if (propertySource == null)
				propertySource = new ConnectionPropertySource(this);
			return propertySource;
		}
		return null;
	}
}
