package org.gsn.model.business;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.util.GsnNameUtil;

@Data
@EqualsAndHashCode(callSuper = true)
public class Context extends AbstractNode {
	private static final long serialVersionUID = -710309047878364862L;

	@Override
	public Object clone() throws CloneNotSupportedException {
		final Context context = new Context();
		context.setColor(this.getRed(), this.getBlue(), this.getGreen());
		context.setName(GsnNameUtil.newGenericName((GsnDiagram) getParent(), Context.class));
		context.setIdentifier(this.getIdentifier());
		context.setAttributes(this.getAttributes());
		context.setDescription(this.getDescription());
		context.setToBeInstantiated(this.isToBeInstantiated());
		context.setParent(this.getParent());
		context.setLayout(new Rectangle(getLayout().x + 10, getLayout().y + 10, getLayout().width, getLayout().height));
		return context;
	}
}
