package org.gsn.model.business;

import static com.google.common.collect.Lists.newArrayList;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.views.properties.IPropertySource;
import org.gsn.model.exception.InvalidWeightsException;
import org.gsn.model.properties.NodePropertySource;

@Data
@EqualsAndHashCode(exclude = { "parent", "sourceConnectionsArray", "targetConnectionsArray", "children", "distribution" })
public abstract class AbstractNode implements IAdaptable, Serializable {
	private static final long serialVersionUID = 5210119720889549030L;

	// properties copied from your class hierarchy.
	// not sure whether we need both name and identifier.
	protected String name;
	private String description;
	private String identifier;
	private String attributes;
	private boolean toBeInstantiated;
	protected final int distLength = 5;
	protected Double[] distribution = new Double[distLength];
	transient protected Map<AbstractNode, Double> children = new HashMap<AbstractNode, Double>();

	// GEF related properties
	private int red = 255;
	private int green = 255;
	private int blue = 255;
	private Rectangle layout;
	private AbstractNode parent = null;
	private List<AbstractNode> childrenNodesArray = newArrayList();
	private List<Connection> sourceConnectionsArray = newArrayList();
	private List<Connection> targetConnectionsArray = newArrayList();
	private IPropertySource propertySource = null;
	private PropertyChangeSupport listeners;
	public static final String PROPERTY_LAYOUT = "NodeLayout";
	public static final String PROPERTY_REMOVE = "NodeRemoveChild";
	public static final String PROPERTY_ADD = "NodeAddChild";
	public static final String PROPERTY_RENAME = "NodeRename";
	public static final String PROPERTY_COLOR = "NodeColor";
	public static final String PROPERTY_IDENTIFIER = "NodeIdentifier";
	public static final String PROPERTY_DESCRIPTION = "NodeDescription";
	public static final String PROPERTY_ATTRIBUTES = "NodeAttributes";
	public static final String PROPERTY_INSTANTIATED = "NodeInstantiated";
	public static final String SOURCE_CONNECTION = "SourceConnectionAdded";
	public static final String TARGET_CONNECTION = "TargetConnectionAdded";

	public boolean isSolution() {
		return false;
	}

	public double getWeight(final AbstractNode child) {
		return children.get(child);
	}

	public void setChildren(final Collection<AbstractNode> children) throws InvalidWeightsException {
		/*if(!checkWeights(children))
			throw new InvalidWeightsException("Weights of children must add up to 1!");
		this.children = children;*/
		this.children = new HashMap<AbstractNode, Double>();
		final double weight = 1D / children.size();
		for (final AbstractNode n : children) {
			this.children.put(n, weight);
		}
	}

	public AbstractNode() {
		this.name = "Unknown";
		this.layout = new Rectangle(10, 10, 100, 100);
		this.listeners = new PropertyChangeSupport(this);
		for (int i = 0; i < distLength; i++) {
			distribution[i] = 0D;
		}
	}

	public void setName(final String name) {
		final String oldName = this.name;
		this.name = name;
		getListeners().firePropertyChange(PROPERTY_RENAME, oldName, this.name);
	}

	public void setDescription(final String description) {
		final String oldDescription = this.description;
		this.description = description;
		getListeners().firePropertyChange(PROPERTY_DESCRIPTION, oldDescription, this.description);
	}

	public void setIdentifier(final String identifier) {
		final String oldIdentifier = this.identifier;
		this.identifier = identifier;
		getListeners().firePropertyChange(PROPERTY_IDENTIFIER, oldIdentifier, this.identifier);
	}

	public void setAttributes(final String attributes) {
		final String oldAttributes = this.attributes;
		this.attributes = attributes;
		getListeners().firePropertyChange(PROPERTY_ATTRIBUTES, oldAttributes, this.attributes);
	}

	public void setToBeInstantiated(final boolean toBeInstantiated) {
		final boolean oldInstantiated = this.toBeInstantiated;
		this.toBeInstantiated = toBeInstantiated;
		getListeners().firePropertyChange(PROPERTY_INSTANTIATED, oldInstantiated, this.toBeInstantiated);
	}

	public void setColor(final int red, final int blue, final int green) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		getListeners().firePropertyChange(PROPERTY_COLOR, null, null);
	}

	public Color getColor() {
		return new Color(null, red, blue, green);
	}

	public void setLayout(final Rectangle layout) {
		final Rectangle oldLayout = this.layout;
		this.layout = layout;
		getListeners().firePropertyChange(PROPERTY_LAYOUT, oldLayout, layout);
	}

	public boolean addChild(final AbstractNode child) {
		final boolean b = this.childrenNodesArray.add(child);
		if (b) {
			child.setParent(this);
			getListeners().firePropertyChange(PROPERTY_ADD, null, child);
		}
		return b;

	}

	public boolean removeChild(final AbstractNode child) {
		final boolean b = this.childrenNodesArray.remove(child);
		if (b)
			getListeners().firePropertyChange(PROPERTY_REMOVE, child, null);
		return b;

	}

	public boolean contains(final AbstractNode child) {
		return childrenNodesArray.contains(child);
	}

	public boolean addConnections(final Connection conn) {
		if (conn.getSourceNode() == this) {
			if (!sourceConnectionsArray.contains(conn)) {
				if (sourceConnectionsArray.add(conn)) {
					getListeners().firePropertyChange(SOURCE_CONNECTION, null, conn);
					return true;
				}
				return false;
			}
		} else if (conn.getTargetNode() == this) {
			if (!targetConnectionsArray.contains(conn)) {
				if (targetConnectionsArray.add(conn)) {
					getListeners().firePropertyChange(TARGET_CONNECTION, null, conn);
					return true;
				}
				return false;
			}
		}
		return false;
	}

	public boolean removeConnection(final Connection conn) {
		if (conn.getSourceNode() == this) {
			if (sourceConnectionsArray.contains(conn)) {
				if (sourceConnectionsArray.remove(conn)) {
					getListeners().firePropertyChange(SOURCE_CONNECTION, null, conn);
					return true;
				}
				return false;
			}
		} else if (conn.getTargetNode() == this) {
			if (targetConnectionsArray.contains(conn)) {
				if (targetConnectionsArray.remove(conn)) {
					getListeners().firePropertyChange(TARGET_CONNECTION, null, conn);
					return true;
				}
				return false;
			}
		}
		return false;
	}

	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		listeners.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(final PropertyChangeListener listener) {
		listeners.removePropertyChangeListener(listener);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(final Class adapter) {
		if (adapter == IPropertySource.class) {
			if (propertySource == null)
				propertySource = new NodePropertySource(this);
			return propertySource;
		}
		return null;
	}
}
