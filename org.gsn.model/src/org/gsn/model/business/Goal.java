package org.gsn.model.business;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.util.GsnNameUtil;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Goal extends AbstractNode {
	private static final long serialVersionUID = -710309047878364862L;

	public Goal(final String name) {
		super();
		setName(name);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		final Goal goal = new Goal();
		goal.setColor(this.getRed(), this.getBlue(), this.getGreen());
		goal.setName(GsnNameUtil.newGenericName((GsnDiagram) getParent(), Goal.class));
		goal.setIdentifier(this.getIdentifier());
		goal.setAttributes(this.getAttributes());
		goal.setDescription(this.getDescription());
		goal.setToBeInstantiated(this.isToBeInstantiated());
		goal.setParent(this.getParent());
		goal.setLayout(new Rectangle(getLayout().x + 10, getLayout().y + 10, getLayout().width, getLayout().height));
		return goal;
	}
}
