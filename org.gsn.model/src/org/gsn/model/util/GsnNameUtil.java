package org.gsn.model.util;

import org.apache.commons.lang3.StringUtils;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Context;
import org.gsn.model.business.Goal;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Solution;
import org.gsn.model.business.Strategy;

public class GsnNameUtil {
	@SuppressWarnings("rawtypes")
	public static String newGenericName(final GsnDiagram diagram, final Class clazz) {
		final String newIndexStr = Integer.toString(getObjectNumberOfType(diagram, clazz));
		if (clazz == Goal.class) {
			return StringUtils.join("G", newIndexStr);
		} else if (clazz == Solution.class) {
			return StringUtils.join("Sn", newIndexStr);
		} else if (clazz == Strategy.class) {
			return StringUtils.join("S", newIndexStr);
		} else if (clazz == Context.class) {
			return StringUtils.join("Con", newIndexStr);
		}
		return "Unknown name";

	}

	@SuppressWarnings("rawtypes")
	private static int getObjectNumberOfType(final GsnDiagram diagram, final Class clazz) {
		int objectNum = 0;
		for (final AbstractNode node : diagram.getChildrenNodesArray()) {
			if (node.getClass() == clazz)
				objectNum += 1;
		}
		return objectNum + 1;
	}
}
