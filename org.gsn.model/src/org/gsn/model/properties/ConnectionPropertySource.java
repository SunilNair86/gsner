package org.gsn.model.properties;

import static com.google.common.collect.Lists.newArrayList;

import java.io.Serializable;
import java.util.List;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;
import org.gsn.model.business.Connection;

public class ConnectionPropertySource implements IPropertySource, Serializable {
	private static final long serialVersionUID = -1597326378691231439L;
	private final Connection connection;

	public ConnectionPropertySource(final Connection connection) {
		this.connection = connection;
	}

	/**
	 * Returns the property value when this property source is used as a value. We can return <tt>null</tt> here
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		final List<IPropertyDescriptor> properties = newArrayList();
		if (connection.getAcp() != null)
			properties.add(new TextPropertyDescriptor(Connection.PROPERTY_ACP_RENAME, "ACP Name"));
		return properties.toArray(new IPropertyDescriptor[properties.size()]);
	}

	@Override
	public Object getPropertyValue(final Object id) {
		if (!(id instanceof String))
			return null;
		final String strId = (String) id;
		if (strId.equals(Connection.PROPERTY_ACP_RENAME))
			return connection.getAcp().getName();

		return null;
	}

	// Returns if the property with the given id has been changed since its
	// initial default value.
	// We do not handle default properties, so we return <tt>false</tt>.
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/**
	 * Reset a property to its default value. Since we do not handle default properties, we do nothing.
	 */
	@Override
	public void resetPropertyValue(final Object id) {}

	@Override
	public void setPropertyValue(final Object id, final Object value) {
		if (!(id instanceof String))
			return;
		final String strId = (String) id;
		if (strId.equals(Connection.PROPERTY_ACP_RENAME))
			connection.setAcpName((String) value);
	}
}
