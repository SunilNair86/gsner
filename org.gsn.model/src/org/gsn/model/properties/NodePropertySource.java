package org.gsn.model.properties;

import static com.google.common.collect.Lists.newArrayList;

import java.io.Serializable;
import java.util.List;

import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.views.properties.ColorPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.GsnDiagram;

public class NodePropertySource implements IPropertySource, Serializable {
	private static final long serialVersionUID = -1597326378691231439L;
	private final AbstractNode node;

	public NodePropertySource(final AbstractNode node) {
		this.node = node;
	}

	/**
	 * Returns the property value when this property source is used as a value. We can return <tt>null</tt> here
	 */
	@Override
	public Object getEditableValue() {
		return null;
	}

	@Override
	public IPropertyDescriptor[] getPropertyDescriptors() {
		final List<IPropertyDescriptor> properties = newArrayList();
		properties.add(new TextPropertyDescriptor(AbstractNode.PROPERTY_RENAME, "Name"));
		if (!(node instanceof GsnDiagram)) {
			properties.add(new TextPropertyDescriptor(AbstractNode.PROPERTY_IDENTIFIER, "Identifier"));
			properties.add(new TextPropertyDescriptor(AbstractNode.PROPERTY_DESCRIPTION, "Description"));
			properties.add(new TextPropertyDescriptor(AbstractNode.PROPERTY_ATTRIBUTES, "Attributes"));
			properties.add(new ColorPropertyDescriptor(AbstractNode.PROPERTY_COLOR, "Color"));
		}
		return properties.toArray(new IPropertyDescriptor[properties.size()]);
	}

	@Override
	public Object getPropertyValue(final Object id) {
		if (!(id instanceof String))
			return null;
		final String strId = (String) id;
		if (strId.equals(AbstractNode.PROPERTY_RENAME))
			return node.getName();
		else if (strId.equals(AbstractNode.PROPERTY_COLOR))
			return node.getColor().getRGB();
		else if (strId.equals(AbstractNode.PROPERTY_DESCRIPTION))
			return node.getDescription();
		else if (strId.equals(AbstractNode.PROPERTY_ATTRIBUTES))
			return node.getAttributes();
		else if (strId.equals(AbstractNode.PROPERTY_IDENTIFIER))
			return node.getIdentifier();

		return null;
	}

	// Returns if the property with the given id has been changed since its
	// initial default value.
	// We do not handle default properties, so we return <tt>false</tt>.
	@Override
	public boolean isPropertySet(final Object id) {
		return false;
	}

	/**
	 * Reset a property to its default value. Since we do not handle default properties, we do nothing.
	 */
	@Override
	public void resetPropertyValue(final Object id) {}

	@Override
	public void setPropertyValue(final Object id, final Object value) {
		if (!(id instanceof String))
			return;
		final String strId = (String) id;
		if (strId.equals(AbstractNode.PROPERTY_RENAME))
			node.setName((String) value);
		else if (strId.equals(AbstractNode.PROPERTY_COLOR)) {
			final RGB newColor = (RGB) value;
			node.setColor(newColor.red, newColor.green, newColor.blue);
		} else if (strId.equals(AbstractNode.PROPERTY_DESCRIPTION))
			node.setDescription((String) value);
		else if (strId.equals(AbstractNode.PROPERTY_ATTRIBUTES))
			node.setAttributes((String) value);
		else if (strId.equals(AbstractNode.PROPERTY_IDENTIFIER))
			node.setIdentifier((String) value);
	}
}
