package org.gsn.model.service;

import org.eclipse.gef.requests.CreationFactory;
import org.gsn.model.business.Context;
import org.gsn.model.business.Goal;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Solution;
import org.gsn.model.business.Strategy;
import org.gsn.model.util.GsnNameUtil;

public class NodeCreationFactory implements CreationFactory {

	private final Class<?> template;
	private GsnDiagram diagram;

	public NodeCreationFactory(final Class<?> t) {
		this.template = t;
	}

	public void setGsnDiagram(final GsnDiagram diagram) {
		this.diagram = diagram;
	}

	@Override
	public Object getNewObject() {
		if (template == null)
			return null;
		if (template == Goal.class) {
			final Goal goal = new Goal();
			goal.setName(GsnNameUtil.newGenericName(diagram, Goal.class));
			goal.setDescription("Description");
			goal.setAttributes("Attributes");
			goal.setIdentifier("Identifier");
			goal.setToBeInstantiated(true);
			return goal;
		} else if (template == Solution.class) {
			final Solution solution = new Solution();
			solution.setName(GsnNameUtil.newGenericName(diagram, Solution.class));
			solution.setDescription("Description");
			solution.setAttributes("Attributes");
			solution.setIdentifier("Identifier");
			solution.setToBeInstantiated(true);
			return solution;
		} else if (template == Strategy.class) {
			final Strategy strategy = new Strategy();
			strategy.setName(GsnNameUtil.newGenericName(diagram, Strategy.class));
			strategy.setDescription("Description");
			strategy.setAttributes("Attributes");
			strategy.setIdentifier("Identifier");
			strategy.setToBeInstantiated(true);
			return strategy;
		} else if (template == Context.class) {
			final Context context = new Context();
			context.setName(GsnNameUtil.newGenericName(diagram, Context.class));
			context.setDescription("Description");
			context.setAttributes("Attributes");
			context.setIdentifier("Identifier");
			context.setToBeInstantiated(true);
			return context;
		}
		return null;
	}

	@Override
	public Object getObjectType() {
		return template;
	}

}