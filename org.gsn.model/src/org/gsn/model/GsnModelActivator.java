package org.gsn.model;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class GsnModelActivator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(final BundleContext bundleContext) throws Exception {
		GsnModelActivator.context = bundleContext;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(final BundleContext bundleContext) throws Exception {
		GsnModelActivator.context = null;
	}

	public static Path getResource(final IPath resourcePath) {
		final URL url = FileLocator.find(getContext().getBundle(), resourcePath, Collections.EMPTY_MAP);
		if (url == null) {
			return null;
		}
		URL fileUrl = null;
		try {
			fileUrl = FileLocator.toFileURL(url);
			return new Path(fileUrl.getPath());
		} catch (final IOException e) {
			return null;
		}
	}
}
