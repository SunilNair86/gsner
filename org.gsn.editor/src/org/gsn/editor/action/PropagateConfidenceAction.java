package org.gsn.editor.action;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.FileEditorInput;
import org.ertool.adaptors.gsn.GSN2BeliefTree;
import org.ertool.model.BeliefDAG;
import org.ertool.view.PlotDAG;
import org.gsn.editor.GsnEditor;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Connection;
import org.gsn.model.business.Goal;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Solution;
import org.gsn.model.exception.InvalidWeightsException;

import com.google.common.collect.Lists;

public class PropagateConfidenceAction extends SelectionAction {

	public PropagateConfidenceAction(final IWorkbenchPart part) {
		super(part);
		setLazyEnablementCalculation(true);
	}

	@Override
	protected void init() {
		setText("Propagate confidence...");
		setToolTipText("Propagate confidence");
		setId(ActionFactory.RENAME.getId());

		setEnabled(false);
	}

	@Override
	protected boolean calculateEnabled() {
		final List<AbstractNode> rootNodes = getRootNodes();
		if (rootNodes.isEmpty() || rootNodes.size() > 1)
			return false;
		return true;
	}

	@Override
	public void run() {
		final List<AbstractNode> rootNodes = getRootNodes();
		if (rootNodes.isEmpty() || rootNodes.size() > 1) {
			MessageDialog.openError(Display.getDefault().getActiveShell(), "GSN Error", "Can not find root goal node");
			return;
		}

		// set model children
		for (final AbstractNode node : getModel().getChildrenNodesArray()) {
			final List<AbstractNode> childrenSet = newArrayList();
			for (final Connection conn : node.getSourceConnectionsArray()) {
				childrenSet.add(conn.getTargetNode());
			}
			try {
				node.setChildren(childrenSet);
				if (node instanceof Solution && !((Solution) node).getResponses().isEmpty()) {
					final List<AbstractNode> childrenSet1 = newArrayList();
					for (final AbstractNode qr : ((Solution) node).getResponses()) {
						childrenSet1.add(qr);
					}
					node.setChildren(childrenSet1);
				}
			} catch (final InvalidWeightsException e) {
				e.printStackTrace();
			}
		}

		// final GSNTester tester = new GSNTester();
		// tester.setGsn(rootNodes.get(0));
		// tester.GSNBeliefDAGTest();
		final IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		final FileEditorInput input = (FileEditorInput) activeEditor.getEditorInput();

		final GSN2BeliefTree gsnConvertor = new GSN2BeliefTree(rootNodes.get(0));
		final BeliefDAG bdg = gsnConvertor.getBeliefDAG();
		final PlotDAG pd = new PlotDAG(bdg, input.getFile().getRawLocation().removeLastSegments(1).append("results")
				.addFileExtension("png"));
		try {
			input.getFile().getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (final CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private List<AbstractNode> getRootNodes() {
		final List<AbstractNode> rootNodes = Lists.newArrayList();
		for (final AbstractNode node : getModel().getChildrenNodesArray()) {
			if (node instanceof Goal && node.getTargetConnectionsArray().isEmpty())
				rootNodes.add(node);
		}
		return rootNodes;
	}

	private GsnDiagram getModel() {
		final IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		final GsnEditor gsnEditor = (GsnEditor) activeEditor;
		return gsnEditor.getModel();
	}
}
