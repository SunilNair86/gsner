package org.gsn.editor.action;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.gsn.editor.wizard.QuantifyConfidencePage;
import org.gsn.editor.wizard.QuantifyConfidenceWizard;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.QuestionResponse;
import org.gsn.model.business.Solution;

import com.google.common.collect.Lists;

public class QuantifyConfidenceAction extends SelectionAction {

	public static final String ID = "quantifyConfidence";

	public QuantifyConfidenceAction(final IWorkbenchPart part) {
		super(part);
		setLazyEnablementCalculation(true);
	}

	@Override
	protected void init() {
		setText("Quantify Confidence...");
		setToolTipText("Quantify Confidence");
		setId(ID);

		final ImageDescriptor icon = AbstractUIPlugin.imageDescriptorFromPlugin("TutoGEF", "icons/rename-icon.png");
		if (icon != null)
			setImageDescriptor(icon);
		setEnabled(false);
	}

	@Override
	protected boolean calculateEnabled() {
		// On laisse les EditPolicy decider si la commande est disponible ou non
		final Command cmd = createQuantifyConfidenceCommand("");
		if (cmd == null)
			return false;
		return true;
	}

	public Command createQuantifyConfidenceCommand(final String name) {
		final Request quantifyConfidenceRequest = new Request(ID);
		final List<Object> selectedObjects = getSelectedObjects();
		if (selectedObjects.isEmpty())
			return null;
		if (!(selectedObjects.get(0) instanceof EditPart))
			return null;
		final Solution selectedNode = getSelectedNode();
		if (selectedNode == null)
			return null;
		final EditPart editPart = (EditPart) selectedObjects.get(0);
		final Command cmd = editPart.getCommand(quantifyConfidenceRequest);
		return cmd;
	}

	@Override
	public void run() {
		final Solution node = getSelectedNode();
		if (node == null) {
			return;
		}
		final QuantifyConfidenceWizard wizard = new QuantifyConfidenceWizard();
		final WizardDialog dialog = new WizardDialog(getWorkbenchPart().getSite().getShell(), wizard);

		dialog.create();
		dialog.getShell().setSize(600, 300);
		dialog.getShell().setText("Quantify confidence wizard");
		if (dialog.open() == WizardDialog.OK) {
			final List<QuantifyConfidencePage> questionPages = wizard.getQuestionPages();
			final List<AbstractNode> responses = Lists.newArrayList();
			for (int i = 0; i < questionPages.size(); i++) {
				final QuantifyConfidencePage page = questionPages.get(i);
				responses.add(new QuestionResponse(page.getName(), Integer.toString(i), page.getScore(), page.getConfidence()));
			}
			node.setResponses(responses);
		}
	}

	// Helper
	private Solution getSelectedNode() {
		final List objects = getSelectedObjects();
		if (objects.isEmpty())
			return null;
		if (!(objects.get(0) instanceof EditPart))
			return null;
		final EditPart part = (EditPart) objects.get(0);
		if (part.getModel() instanceof Solution) {
			final Solution solution = (Solution) part.getModel();
			if (solution.getSourceConnectionsArray().isEmpty())
				return solution;
		}
		return null;
	}

}
