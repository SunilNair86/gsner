package org.gsn.editor.editpolicy;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.gsn.editor.command.QuantifyConfidenceCommand;
import org.gsn.model.business.Solution;

public class AppQuantifyConfidencePolicy extends AbstractEditPolicy {
	@Override
	public Command getCommand(final Request request) {
		if (request.getType().equals("quantifyConfidence"))
			return createQuantifyConfidenceCommand(request);
		return null;
	}

	protected Command createQuantifyConfidenceCommand(final Request quantifyConfidenceRequest) {
		final QuantifyConfidenceCommand command = new QuantifyConfidenceCommand();
		command.setSolution((Solution) getHost().getModel());
		return command;
	}
}
