package org.gsn.editor.editpolicy;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.gsn.editor.command.AbstractLayoutCommand;
import org.gsn.editor.command.ContextChangeLayoutCommand;
import org.gsn.editor.command.ContextCreateCommand;
import org.gsn.editor.command.GoalChangeLayoutCommand;
import org.gsn.editor.command.GoalCreateCommand;
import org.gsn.editor.command.SolutionChangeLayoutCommand;
import org.gsn.editor.command.SolutionCreateCommand;
import org.gsn.editor.command.StrategyChangeLayoutCommand;
import org.gsn.editor.command.StrategyCreateCommand;
import org.gsn.editor.editpart.ConnectionPart;
import org.gsn.editor.editpart.ContextPart;
import org.gsn.editor.editpart.GoalPart;
import org.gsn.editor.editpart.GsnDiagramPart;
import org.gsn.editor.editpart.SolutionPart;
import org.gsn.editor.editpart.StrategyPart;
import org.gsn.editor.figure.ContextFigure;
import org.gsn.editor.figure.GoalFigure;
import org.gsn.editor.figure.SolutionFigure;
import org.gsn.editor.figure.StrategyFigure;
import org.gsn.model.business.ACP;
import org.gsn.model.business.Connection;
import org.gsn.model.business.Context;
import org.gsn.model.business.Goal;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Solution;
import org.gsn.model.business.Strategy;

public class AppEditLayoutPolicy extends XYLayoutEditPolicy {

	@Override
	protected Command createChangeConstraintCommand(final EditPart child, final Object constraint) {
		AbstractLayoutCommand command = null;

		if (child instanceof SolutionPart) {
			command = new SolutionChangeLayoutCommand();
		} else if (child instanceof GoalPart) {
			command = new GoalChangeLayoutCommand();
		} else if (child instanceof StrategyPart) {
			command = new StrategyChangeLayoutCommand();
		} else if (child instanceof ContextPart) {
			command = new ContextChangeLayoutCommand();
		}

		command.setModel(child.getModel());
		command.setConstraint((Rectangle) constraint);
		return command;
	}

	@Override
	protected Command getCreateCommand(final CreateRequest request) {
		final Object childClass = request.getNewObjectType();
		if (request.getType() == REQ_CREATE && getHost() instanceof GsnDiagramPart && childClass == Goal.class) {
			final GoalCreateCommand cmd = new GoalCreateCommand();
			cmd.setGsnDiagram(getHost().getModel());
			cmd.setGoal(request.getNewObject());

			final Rectangle constraint = (Rectangle) getConstraintFor(request);
			constraint.x = (constraint.x < 0) ? 0 : constraint.x;
			constraint.y = (constraint.y < 0) ? 0 : constraint.y;
			constraint.width = (constraint.width <= 0) ? GoalFigure.GOAL_FIGURE_DEFWIDTH : constraint.width;
			constraint.height = (constraint.height <= 0) ? GoalFigure.GOAL_FIGURE_DEFHEIGHT : constraint.height;
			cmd.setLayout(constraint);
			return cmd;
		} else if (request.getType() == REQ_CREATE && getHost() instanceof GsnDiagramPart && childClass == Context.class) {
			final ContextCreateCommand cmd = new ContextCreateCommand();
			cmd.setGsnDiagram(getHost().getModel());
			cmd.setContext(request.getNewObject());

			final Rectangle constraint = (Rectangle) getConstraintFor(request);
			constraint.x = (constraint.x < 0) ? 0 : constraint.x;
			constraint.y = (constraint.y < 0) ? 0 : constraint.y;
			constraint.width = (constraint.width <= 0) ? ContextFigure.CONTEXT_FIGURE_DEFWIDTH : constraint.width;
			constraint.height = (constraint.height <= 0) ? ContextFigure.CONTEXT_FIGURE_DEFHEIGHT : constraint.height;
			cmd.setLayout(constraint);
			return cmd;
		} else if (request.getType() == REQ_CREATE && getHost() instanceof GsnDiagramPart && childClass == Strategy.class) {
			final StrategyCreateCommand cmd = new StrategyCreateCommand();
			cmd.setGsnDiagram(getHost().getModel());
			cmd.setStrategy(request.getNewObject());

			final Rectangle constraint = (Rectangle) getConstraintFor(request);
			constraint.x = (constraint.x < 0) ? 0 : constraint.x;
			constraint.y = (constraint.y < 0) ? 0 : constraint.y;
			constraint.width = (constraint.width <= 0) ? StrategyFigure.STRATEGY_FIGURE_DEFWIDTH : constraint.width;
			constraint.height = (constraint.height <= 0) ? StrategyFigure.STRATEGY_FIGURE_DEFHEIGHT : constraint.height;
			cmd.setLayout(constraint);
			return cmd;
		} else if (request.getType() == REQ_CREATE && getHost() instanceof GsnDiagramPart && childClass == Solution.class) {
			final SolutionCreateCommand cmd = new SolutionCreateCommand();
			cmd.setGsnDiagram(getHost().getModel());
			cmd.setSolution(request.getNewObject());

			final Rectangle constraint = (Rectangle) getConstraintFor(request);
			constraint.x = (constraint.x < 0) ? 0 : constraint.x;
			constraint.y = (constraint.y < 0) ? 0 : constraint.y;
			constraint.width = (constraint.width <= 0) ? SolutionFigure.SOLUTION_FIGURE_DEFWIDTH : constraint.width;
			constraint.height = (constraint.height <= 0) ? SolutionFigure.SOLUTION_FIGURE_DEFHEIGHT : constraint.height;
			cmd.setLayout(constraint);
			return cmd;
		} else if (request.getType() == REQ_CREATE && getHost() instanceof ConnectionPart && childClass == ACP.class) {
			return new Command() {
				@Override
				public void execute() {
					super.execute();
					final GsnDiagram acp = new GsnDiagram();
					acp.setName("ACP1");
					((Connection) (getHost().getModel())).setAcp(acp);
				}
			};
		}

		return null;
	}
}
