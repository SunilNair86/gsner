package org.gsn.editor.editpolicy;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;
import org.gsn.editor.command.ConnectionCreateCommand;
import org.gsn.editor.command.ConnectionReconnectCommand;
import org.gsn.model.business.Connection;
import org.gsn.model.business.AbstractNode;

public class AppConnectionPolicy extends GraphicalNodeEditPolicy {

	@Override
	protected Command getConnectionCompleteCommand(
			CreateConnectionRequest request) {
		ConnectionCreateCommand cmd = (ConnectionCreateCommand) request
				.getStartCommand();
		AbstractNode targetNode = (AbstractNode) getHost().getModel();
		cmd.setTargetNode(targetNode);
		return cmd;
	}

	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request) {
		ConnectionCreateCommand cmd = new ConnectionCreateCommand();
		AbstractNode sourceNode = (AbstractNode) getHost().getModel();
		cmd.setConnectionType(Integer.parseInt(request.getNewObjectType()
				.toString()));
		cmd.setSourceNode(sourceNode);
		request.setStartCommand(cmd);
		return cmd;
	}

	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request) {
		Connection conn = (Connection) request.getConnectionEditPart()
				.getModel();
		AbstractNode sourceNode = (AbstractNode) getHost().getModel();
		ConnectionReconnectCommand cmd = new ConnectionReconnectCommand(conn);
		cmd.setNewSourceNode(sourceNode);
		return cmd;
	}

	@Override
	protected Command getReconnectTargetCommand(ReconnectRequest request) {
		Connection conn = (Connection) request.getConnectionEditPart()
				.getModel();
		AbstractNode targetNode = (AbstractNode) getHost().getModel();
		ConnectionReconnectCommand cmd = new ConnectionReconnectCommand(conn);
		cmd.setNewTargetNode(targetNode);
		return cmd;
	}
}
