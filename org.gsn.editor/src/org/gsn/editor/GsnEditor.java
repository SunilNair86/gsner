package org.gsn.editor;

import static com.google.common.collect.Lists.newArrayList;
import static org.eclipse.ui.plugin.AbstractUIPlugin.imageDescriptorFromPlugin;
import static org.gsn.editor.GsnActivator.PLUGIN_ID;
import static org.gsn.model.business.Connection.CONNECTION_DESIGN;
import static org.gsn.model.business.Connection.CONNECTION_RESOURCES;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.EventObject;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.parts.ScrollableThumbnail;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.dnd.TemplateTransferDragSourceListener;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.SelectionToolEntry;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.actions.ZoomInAction;
import org.eclipse.gef.ui.actions.ZoomOutAction;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.gef.ui.palette.PaletteViewerProvider;
import org.eclipse.gef.ui.parts.ContentOutlinePage;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.parts.TreeViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.gsn.editor.action.CopyNodeAction;
import org.gsn.editor.action.PasteNodeAction;
import org.gsn.editor.action.PropagateConfidenceAction;
import org.gsn.editor.action.QuantifyConfidenceAction;
import org.gsn.editor.dnd.GsnTemplateTransferDropTargetListener;
import org.gsn.editor.editpart.AppEditPartFactory;
import org.gsn.editor.editpart.ConnectionCreationFactory;
import org.gsn.editor.editpart.tree.AppTreeEditPartFactory;
import org.gsn.model.business.ACP;
import org.gsn.model.business.Context;
import org.gsn.model.business.Goal;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Solution;
import org.gsn.model.business.Strategy;
import org.gsn.model.service.NodeCreationFactory;

public class GsnEditor extends GraphicalEditorWithFlyoutPalette {

	public static final String ID = "org.gsn.editor.gsnEditor";
	@Getter @Setter private GsnDiagram model = new GsnDiagram();
	private KeyHandler keyHandler;
	boolean editorSaving = false;
	private GsnTemplateTransferDropTargetListener dropTargetListener;
	private NodeCreationFactory goalFactory;
	private NodeCreationFactory solutionFactory;
	private NodeCreationFactory strategyFactory;
	private NodeCreationFactory contextFactory;
	private IFile file;

	public GsnEditor() {
		setEditDomain(new DefaultEditDomain(this));
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		final GraphicalViewer viewer = getGraphicalViewer();
		viewer.setEditPartFactory(new AppEditPartFactory());

		final ScalableRootEditPart rootEditPart = new ScalableRootEditPart();
		viewer.setRootEditPart(rootEditPart);

		final ZoomManager manager = rootEditPart.getZoomManager();
		getActionRegistry().registerAction(new ZoomInAction(manager));
		getActionRegistry().registerAction(new ZoomOutAction(manager));

		final double[] zoomLevels = new double[] { 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 10.0, 20.0 };
		manager.setZoomLevels(zoomLevels);

		final List<String> zoomContributions = newArrayList();
		zoomContributions.add(ZoomManager.FIT_ALL);
		zoomContributions.add(ZoomManager.FIT_HEIGHT);
		zoomContributions.add(ZoomManager.FIT_WIDTH);
		manager.setZoomLevelContributions(zoomContributions);

		keyHandler = new KeyHandler();
		keyHandler.put(KeyStroke.getPressed(SWT.DEL, 127, 0), getActionRegistry().getAction(ActionFactory.DELETE.getId()));

		keyHandler.put(KeyStroke.getPressed('+', SWT.KEYPAD_ADD, 0), getActionRegistry().getAction(GEFActionConstants.ZOOM_IN));

		keyHandler.put(KeyStroke.getPressed('-', SWT.KEYPAD_SUBTRACT, 0),
				getActionRegistry().getAction(GEFActionConstants.ZOOM_OUT));

		viewer.setKeyHandler(keyHandler);

		final ContextMenuProvider provider = new AppContextMenuProvider(viewer, getActionRegistry());
		viewer.setContextMenu(provider);

	}

	@Override
	protected void initializeGraphicalViewer() {
		final GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(model);
		dropTargetListener = new GsnTemplateTransferDropTargetListener(viewer);
		dropTargetListener.setGsnDiagram(model);
		viewer.addDropTargetListener(dropTargetListener);
	}

	@Override
	protected PaletteViewerProvider createPaletteViewerProvider() {
		return new PaletteViewerProvider(getEditDomain()) {
			@Override
			protected void configurePaletteViewer(final PaletteViewer viewer) {
				super.configurePaletteViewer(viewer);
				viewer.addDragSourceListener(new TemplateTransferDragSourceListener(viewer));
			}
		};
	}

	@Override
	@SuppressWarnings("unchecked")
	public void createActions() {
		super.createActions();

		final ActionRegistry registry = getActionRegistry();
		IAction action = new PropagateConfidenceAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new QuantifyConfidenceAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new CopyNodeAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		action = new PasteNodeAction(this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
	}

	@Override
	protected PaletteRoot getPaletteRoot() {
		final PaletteRoot root = new PaletteRoot();

		final PaletteGroup manipGroup = new PaletteGroup("Objects selection");
		root.add(manipGroup);

		final SelectionToolEntry selectionToolEntry = new SelectionToolEntry();
		manipGroup.add(selectionToolEntry);
		manipGroup.add(new MarqueeToolEntry());

		final PaletteDrawer instGroup = new PaletteDrawer("GSN Elements");
		root.add(instGroup);

		goalFactory = new NodeCreationFactory(Goal.class);
		instGroup.add(new CombinedTemplateCreationEntry("Goal", "Creates new goal object", Goal.class, goalFactory,
				imageDescriptorFromPlugin(PLUGIN_ID, "icons/Goal.gif"), imageDescriptorFromPlugin(PLUGIN_ID, "icons/Goal.gif")));

		solutionFactory = new NodeCreationFactory(Solution.class);
		instGroup.add(new CombinedTemplateCreationEntry("Solution", "Creates new solution object", Solution.class,
				solutionFactory, imageDescriptorFromPlugin(PLUGIN_ID, "icons/Solution.gif"), imageDescriptorFromPlugin(PLUGIN_ID,
						"icons/Solution.gif")));

		strategyFactory = new NodeCreationFactory(Strategy.class);
		instGroup.add(new CombinedTemplateCreationEntry("Strategy", "Creates new strategy object", Strategy.class,
				strategyFactory, imageDescriptorFromPlugin(PLUGIN_ID, "icons/Strategy.gif"), imageDescriptorFromPlugin(PLUGIN_ID,
						"icons/Strategy.gif")));

		contextFactory = new NodeCreationFactory(Context.class);
		instGroup.add(new CombinedTemplateCreationEntry("Context", "Creates new context object", Context.class, contextFactory,
				imageDescriptorFromPlugin(PLUGIN_ID, "icons/Context.gif"), imageDescriptorFromPlugin(PLUGIN_ID,
						"icons/Context.gif")));

		instGroup.add(new CombinedTemplateCreationEntry("ACP", "Creates new ACP object", ACP.class, new NodeCreationFactory(
				ACP.class), imageDescriptorFromPlugin(PLUGIN_ID, "icons/ACP.gif"), imageDescriptorFromPlugin(PLUGIN_ID,
				"icons/ACP.gif")));

		final PaletteDrawer connectionElements = new PaletteDrawer("GSN Connections");
		root.add(connectionElements);

		connectionElements.add(new ConnectionCreationToolEntry("Supported by", "Creates new 'SupportedBy' connection",
				new ConnectionCreationFactory(CONNECTION_DESIGN), imageDescriptorFromPlugin(PLUGIN_ID, "icons/Link1.gif"),
				imageDescriptorFromPlugin(PLUGIN_ID, "icons/Link1.gif")));

		connectionElements.add(new ConnectionCreationToolEntry("In context of", "Creates new 'InContextOf' connection",
				new ConnectionCreationFactory(CONNECTION_RESOURCES), imageDescriptorFromPlugin(PLUGIN_ID, "icons/Link2.gif"),
				imageDescriptorFromPlugin(PLUGIN_ID, "icons/Link2.gif")));

		root.setDefaultEntry(selectionToolEntry);
		return root;
	}

	@Override
	public void doSave(final IProgressMonitor monitor) {
		/*editorSaving = true;
		final File file = ((GsnEditorInput) getEditorInput()).getFile();
		if (file != null) {
			WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
				public void execute(final IProgressMonitor monitor) {
					try {
						ByteArrayOutputStream outStream = new ByteArrayOutputStream();
						ObjectOutputStream out = new ObjectOutputStream(outStream);
						out.writeObject(getModel());
						FileOutputStream fos = new FileOutputStream(file);
						fos.write(outStream.toByteArray());
						fos.close();
						out.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			try {
				new ProgressMonitorDialog(getSite().getWorkbenchWindow().getShell()).run(false, true, op);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		editorSaving = false;*/

		try {
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			createOutputStream(out);
			final IFile file = ((IFileEditorInput) getEditorInput()).getFile();
			file.setContents(new ByteArrayInputStream(out.toByteArray()), true, false, monitor);
			out.close();
			getCommandStack().markSaveLocation();
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	protected void createOutputStream(final OutputStream os) throws IOException {
		final ObjectOutputStream out = new ObjectOutputStream(os);
		out.writeObject(model);
		out.close();
	}

	/**
	 * @see org.eclipse.gef.commands.CommandStackListener#commandStackChanged(java.util.EventObject)
	 */
	@Override
	public void commandStackChanged(final EventObject event) {
		firePropertyChange(IEditorPart.PROP_DIRTY);
		super.commandStackChanged(event);
	}

	@Override
	protected void setInput(final IEditorInput input) {
		super.setInput(input);

		file = ((IFileEditorInput) input).getFile();
		try {
			final InputStream is = file.getContents(false);
			final ObjectInputStream ois = new ObjectInputStream(is);
			model = (GsnDiagram) ois.readObject();
			ois.close();
		} catch (final Exception e) {
			// model = new GsnDiagram();
		}
		goalFactory.setGsnDiagram(model);
		solutionFactory.setGsnDiagram(model);
		strategyFactory.setGsnDiagram(model);
		contextFactory.setGsnDiagram(model);
	}

	@Override
	public String getTitle() {
		if (file != null) {
			setPartName(file.getName());
			return file.getName();
		}

		return super.getTitle();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(final Class type) {
		if (type == ZoomManager.class)
			return ((ScalableRootEditPart) getGraphicalViewer().getRootEditPart()).getZoomManager();
		if (type == IContentOutlinePage.class) {
			return new OutlinePage();
		}
		return super.getAdapter(type);

	}

	protected class OutlinePage extends ContentOutlinePage {

		private SashForm sash;
		private ScrollableThumbnail thumbnail;
		private DisposeListener disposeListener;

		public OutlinePage() {
			super(new TreeViewer());
		}

		@Override
		public void createControl(final Composite parent) {
			sash = new SashForm(parent, SWT.VERTICAL);

			getViewer().createControl(sash);

			getViewer().setEditDomain(getEditDomain());
			getViewer().setEditPartFactory(new AppTreeEditPartFactory());
			getViewer().setContents(model);

			getSelectionSynchronizer().addViewer(getViewer());

			final Canvas canvas = new Canvas(sash, SWT.BORDER);
			final LightweightSystem lws = new LightweightSystem(canvas);

			thumbnail = new ScrollableThumbnail(
					(Viewport) ((ScalableRootEditPart) getGraphicalViewer().getRootEditPart()).getFigure());
			thumbnail.setSource(((ScalableRootEditPart) getGraphicalViewer().getRootEditPart())
					.getLayer(LayerConstants.PRINTABLE_LAYERS));

			lws.setContents(thumbnail);

			disposeListener = new DisposeListener() {
				@Override
				public void widgetDisposed(final DisposeEvent e) {
					if (thumbnail != null) {
						thumbnail.deactivate();
						thumbnail = null;
					}
				}
			};
			getGraphicalViewer().getControl().addDisposeListener(disposeListener);

			final IActionBars bars = getSite().getActionBars();
			final ActionRegistry ar = getActionRegistry();
			bars.setGlobalActionHandler(ActionFactory.COPY.getId(), ar.getAction(ActionFactory.COPY.getId()));
			bars.setGlobalActionHandler(ActionFactory.PASTE.getId(), ar.getAction(ActionFactory.PASTE.getId()));

		}

		@Override
		public void init(final IPageSite pageSite) {
			super.init(pageSite);

			final IActionBars bars = getSite().getActionBars();
			bars.setGlobalActionHandler(ActionFactory.UNDO.getId(), getActionRegistry().getAction(ActionFactory.UNDO.getId()));
			bars.setGlobalActionHandler(ActionFactory.REDO.getId(), getActionRegistry().getAction(ActionFactory.REDO.getId()));
			bars.setGlobalActionHandler(ActionFactory.DELETE.getId(), getActionRegistry().getAction(ActionFactory.DELETE.getId()));
			bars.updateActionBars();

			getViewer().setKeyHandler(keyHandler);

			final ContextMenuProvider provider = new AppContextMenuProvider(getViewer(), getActionRegistry());
			getViewer().setContextMenu(provider);

		}

		@Override
		public Control getControl() {
			return sash;
		}

		@Override
		public void dispose() {
			getSelectionSynchronizer().removeViewer(getViewer());
			if (getGraphicalViewer().getControl() != null && !getGraphicalViewer().getControl().isDisposed())
				getGraphicalViewer().getControl().removeDisposeListener(disposeListener);
			super.dispose();
		}
	}
}
