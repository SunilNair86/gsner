package org.gsn.editor;

import java.io.File;

import lombok.Data;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

@Data
public class GsnEditorInput implements IEditorInput {
	public String name = null;
	private File file = null;

	public GsnEditorInput(String name) {
		this.name = name;
		file = new File(ResourcesPlugin.getWorkspace().getRoot()
				.getRawLocation()
				+ "/" + name + ".session.saved");
	}

	@Override
	public boolean exists() {
		return (this.name != null);
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return ImageDescriptor.getMissingImageDescriptor();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	@Override
	public String getToolTipText() {
		return this.name;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter) {
		return null;
	}

}
