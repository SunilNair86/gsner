package org.gsn.editor.dnd;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;
import org.eclipse.gef.requests.CreationFactory;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.service.NodeCreationFactory;

public class GsnTemplateTransferDropTargetListener extends TemplateTransferDropTargetListener {

	private GsnDiagram diagram;

	public GsnTemplateTransferDropTargetListener(final EditPartViewer viewer) {
		super(viewer);
	}

	public void setGsnDiagram(final GsnDiagram diagram) {
		this.diagram = diagram;
	}

	@Override
	protected CreationFactory getFactory(final Object template) {
		final NodeCreationFactory nodeCreationFactory = new NodeCreationFactory((Class<?>) template);
		nodeCreationFactory.setGsnDiagram(diagram);
		return nodeCreationFactory;
	}

}
