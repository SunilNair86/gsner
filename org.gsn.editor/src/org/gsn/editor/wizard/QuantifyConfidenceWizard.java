package org.gsn.editor.wizard;

import static com.google.common.collect.Lists.newArrayList;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.wizard.Wizard;
import org.gsn.model.GsnModelActivator;

public class QuantifyConfidenceWizard extends Wizard {

	private final List<QuantifyConfidencePage> questionPages = newArrayList();

	public QuantifyConfidenceWizard() {
		final Path questionsFile = GsnModelActivator.getResource(new Path("/resources/questions.txt"));
		try {
			final List<String> questions = FileUtils.readLines(questionsFile.toFile());
			int index = 1;
			for (final String question : questions) {
				final QuantifyConfidencePage page = new QuantifyConfidencePage(question, index++);
				questionPages.add(page);
				addPage(page);
			}
		} catch (final IOException e) {
			return;
		}
	}

	@Override
	public boolean performFinish() {
		return true;
	}

	public List<QuantifyConfidencePage> getQuestionPages() {
		return questionPages;
	}
}
