package org.gsn.editor.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class QuantifyConfidencePage extends WizardPage {

	private Button rb0;
	private Button rb1;
	private Button rb2;
	private Button rb3;
	private Button rb4;
	private int answer = 1;
	private int confidenceIndex = 0;
	private Combo cmb;
	private Text descriptionText;

	protected QuantifyConfidencePage(final String question, final int index) {
		super(question);
		this.setTitle("Q" + index + ": " + question);
		this.setDescription("Please provide your answer and confidence");
	}

	@Override
	public void createControl(final Composite parent) {
		final Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(6, false));

		new Label(composite, SWT.NONE).setText("Answer");
		rb0 = new Button(composite, SWT.RADIO);
		rb1 = new Button(composite, SWT.RADIO);
		rb2 = new Button(composite, SWT.RADIO);
		rb3 = new Button(composite, SWT.RADIO);
		rb4 = new Button(composite, SWT.RADIO);

		new Label(composite, SWT.NONE).setText("Confidence");
		cmb = new Combo(composite, SWT.BORDER | SWT.READ_ONLY);
		final GridData layoutData = new GridData(SWT.BEGINNING, SWT.FILL, false, false, 5, 1);
		layoutData.minimumWidth = layoutData.widthHint = 75;
		cmb.setLayoutData(layoutData);
		for (int i = 0; i <= 100; i++) {
			cmb.add(i + " %");
		}
		new Label(composite, SWT.NONE).setText("Description");
		descriptionText = new Text(composite, SWT.BORDER | SWT.MULTI);
		descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));

		rb0.setText("0");
		rb1.setText("1");
		rb2.setText("2");
		rb3.setText("3");
		rb4.setText("4");

		rb0.setSelection(true);
		cmb.select(0);

		setListeners();
		setControl(composite);
	}

	private void setListeners() {
		cmb.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				confidenceIndex = cmb.getSelectionIndex();
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				confidenceIndex = cmb.getSelectionIndex();
			}
		});

		rb0.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				answer = 0;
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				answer = 0;
			}
		});

		rb1.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				answer = 1;
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				answer = 1;
			}
		});

		rb2.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				answer = 2;
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				answer = 2;
			}
		});

		rb3.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				answer = 3;
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				answer = 3;
			}
		});

		rb4.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				answer = 4;
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				answer = 4;
			}
		});
	}

	public int getScore() {
		return answer;
	}

	public double getConfidence() {
		return confidenceIndex / 100D;
	}
}
