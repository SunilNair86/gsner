package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.business.Goal;
import org.gsn.model.business.Strategy;

public class StrategyChangeLayoutCommand extends AbstractLayoutCommand {

	private Strategy model;
	private Rectangle layout;
	private Rectangle oldLayout;

	public void execute() {
		model.setLayout(layout);
	}

	public void setConstraint(Rectangle rect) {
		this.layout = rect;
	}

	public void setModel(Object model) {
		this.model = (Strategy) model;
		this.oldLayout = ((Strategy) model).getLayout();
	}

	public void undo() {
		this.model.setLayout(this.oldLayout);
	}

}
