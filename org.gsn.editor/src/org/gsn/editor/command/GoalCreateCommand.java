package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Goal;

public class GoalCreateCommand extends Command {
	private GsnDiagram gsnDiagram;
	private Goal goal;

	public GoalCreateCommand() {
		super();
		gsnDiagram = null;
		goal = null;
	}

	public void setGoal(Object s) {
		if (s instanceof Goal)
			this.goal = (Goal) s;
	}

	public void setGsnDiagram(Object e) {
		if (e instanceof GsnDiagram)
			this.gsnDiagram = (GsnDiagram) e;
	}

	public void setLayout(Rectangle r) {
		if (goal == null)
			return;
		goal.setLayout(r);
	}

	@Override
	public boolean canExecute() {
		if (goal == null || gsnDiagram == null)
			return false;
		return true;
	}

	@Override
	public void execute() {
		gsnDiagram.addChild(goal);
	}

	@Override
	public boolean canUndo() {
		if (gsnDiagram == null || goal == null)
			return false;
		return gsnDiagram.contains(goal);
	}

	@Override
	public void undo() {
		gsnDiagram.removeChild(goal);
	}

}
