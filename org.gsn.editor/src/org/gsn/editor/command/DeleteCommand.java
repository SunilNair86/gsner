package org.gsn.editor.command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Connection;

import com.google.common.collect.Lists;

public class DeleteCommand extends Command {
	private AbstractNode model;
	private AbstractNode parentModel;
	private List<Connection> sourceConnectionsArray;
	private List<Connection> targetConnectionsArray;
	private boolean wasRemoved;

	@Override
	public void execute() {
		sourceConnectionsArray = model.getSourceConnectionsArray();
		targetConnectionsArray = model.getTargetConnectionsArray();
		redo();
	}

	public void setModel(final Object model) {
		this.model = (AbstractNode) model;
	}

	public void setParentModel(final Object model) {
		parentModel = (AbstractNode) model;
	}

	@Override
	public void undo() {
		if (this.parentModel.addChild(model)) {
			addConnections(sourceConnectionsArray);
			addConnections(targetConnectionsArray);
		}
	}

	@Override
	public void redo() {
		wasRemoved = parentModel.removeChild(model);
		if (wasRemoved) {
			removeConnections(sourceConnectionsArray);
			removeConnections(targetConnectionsArray);
		}
	}

	private void removeConnections(final List<Connection> connections) {
		final ArrayList<Connection> newArrayList = Lists.newArrayList(connections);
		for (final Connection conn : newArrayList) {
			conn.disconnect();
		}
	}

	private void addConnections(final List connections) {
		for (final Iterator iter = connections.iterator(); iter.hasNext();) {
			final Connection conn = (Connection) iter.next();
			conn.reconnect(model, parentModel);
		}
	}
}
