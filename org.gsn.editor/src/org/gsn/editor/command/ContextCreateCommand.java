package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.gsn.model.business.Context;
import org.gsn.model.business.GsnDiagram;

public class ContextCreateCommand extends Command {
	private GsnDiagram gsnDiagram;
	private Context context;

	public ContextCreateCommand() {
		super();
		gsnDiagram = null;
		context = null;
	}

	public void setContext(Object s) {
		if (s instanceof Context)
			this.context = (Context) s;
	}

	public void setGsnDiagram(Object e) {
		if (e instanceof GsnDiagram)
			this.gsnDiagram = (GsnDiagram) e;
	}

	public void setLayout(Rectangle r) {
		if (context == null)
			return;
		context.setLayout(r);
	}

	@Override
	public boolean canExecute() {
		if (context == null || gsnDiagram == null)
			return false;
		return true;
	}

	@Override
	public void execute() {
		gsnDiagram.addChild(context);
	}

	@Override
	public boolean canUndo() {
		if (gsnDiagram == null || context == null)
			return false;
		return gsnDiagram.contains(context);
	}

	@Override
	public void undo() {
		gsnDiagram.removeChild(context);
	}

}
