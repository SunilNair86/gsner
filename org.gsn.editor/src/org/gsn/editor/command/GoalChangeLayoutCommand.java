package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.business.Goal;

public class GoalChangeLayoutCommand extends AbstractLayoutCommand {

	private Goal model;
	private Rectangle layout;
	private Rectangle oldLayout;

	public void execute() {
		model.setLayout(layout);
	}

	public void setConstraint(Rectangle rect) {
		this.layout = rect;
	}

	public void setModel(Object model) {
		this.model = (Goal) model;
		this.oldLayout = ((Goal) model).getLayout();
	}

	public void undo() {
		this.model.setLayout(this.oldLayout);
	}

}
