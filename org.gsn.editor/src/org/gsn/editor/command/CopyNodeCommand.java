package org.gsn.editor.command;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.Clipboard;
import org.gsn.model.business.*;

public class CopyNodeCommand extends Command {
	private ArrayList<AbstractNode> list = new ArrayList<AbstractNode>();

	public boolean addElement(AbstractNode node) {
		if (!list.contains(node)) {
			return list.add(node);
		}
		return false;
	}

	@Override
	public boolean canExecute() {
		if (list == null || list.isEmpty())
			return false;
		Iterator<AbstractNode> it = list.iterator();
		while (it.hasNext()) {
			if (!isCopyableNode(it.next()))
				return false;
		}
		return true;
	}

	@Override
	public void execute() {
		if (canExecute())
			Clipboard.getDefault().setContents(list);
	}

	@Override
	public boolean canUndo() {
		return false;
	}

	public boolean isCopyableNode(AbstractNode node) {
		if (node instanceof Goal || node instanceof Solution
				|| node instanceof Strategy || node instanceof Context)
			return true;
		return false;
	}

}
