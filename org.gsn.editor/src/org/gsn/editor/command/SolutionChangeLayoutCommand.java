package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.business.Solution;

public class SolutionChangeLayoutCommand extends AbstractLayoutCommand {
	private Solution model;
	private Rectangle layout;
	private Rectangle oldLayout;

	public void execute() {
		model.setLayout(layout);
	}

	public void setConstraint(Rectangle rect) {
		this.layout = rect;
	}

	public void setModel(Object model) {
		this.model = (Solution) model;
		this.oldLayout = ((Solution) model).getLayout();

	}

	public void undo() {
		this.model.setLayout(this.oldLayout);
	}

}
