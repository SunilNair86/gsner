package org.gsn.editor.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.Clipboard;
import org.gsn.model.business.*;

public class PasteNodeCommand extends Command {
	private HashMap<AbstractNode, AbstractNode> list = new HashMap<AbstractNode, AbstractNode>();

	@Override
	public boolean canExecute() {
		ArrayList<AbstractNode> bList = (ArrayList<AbstractNode>) Clipboard.getDefault()
				.getContents();
		if (bList == null || bList.isEmpty())
			return false;

		Iterator<AbstractNode> it = bList.iterator();
		while (it.hasNext()) {
			AbstractNode node = (AbstractNode) it.next();
			if (isPastableNode(node)) {
				list.put(node, null);
			}
		}
		return true;
	}

	@Override
	public void execute() {
		if (!canExecute())
			return;

		Iterator<AbstractNode> it = list.keySet().iterator();
		while (it.hasNext()) {
			AbstractNode node = (AbstractNode) it.next();
			try {
				if (node instanceof Goal) {
					Goal srv = (Goal) node;
					Goal clone = (Goal) srv.clone();
					list.put(node, clone);
				} else if (node instanceof Solution) {
					Solution emp = (Solution) node;
					Solution clone = (Solution) emp.clone();
					list.put(node, clone);
				} else if (node instanceof Strategy) {
					Strategy emp = (Strategy) node;
					Strategy clone = (Strategy) emp.clone();
					list.put(node, clone);
				} else if (node instanceof Context) {
					Context emp = (Context) node;
					Context clone = (Context) emp.clone();
					list.put(node, clone);
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}

		redo();
	}

	@Override
	public void redo() {
		Iterator<AbstractNode> it = list.values().iterator();
		while (it.hasNext()) {
			AbstractNode node = it.next();
			if (isPastableNode(node)) {
				node.getParent().addChild(node);
			}
		}
	}

	@Override
	public boolean canUndo() {
		return !(list.isEmpty());
	}

	@Override
	public void undo() {
		Iterator<AbstractNode> it = list.values().iterator();
		while (it.hasNext()) {
			AbstractNode node = it.next();
			if (isPastableNode(node)) {
				node.getParent().removeChild(node);
			}
		}
	}

	public boolean isPastableNode(AbstractNode node) {
		if (node instanceof Goal || node instanceof Solution
				|| node instanceof Strategy || node instanceof Context)
			return true;
		return false;
	}

}
