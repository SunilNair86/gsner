package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Strategy;

public class StrategyCreateCommand extends Command {
	private GsnDiagram gsnDiagram;
	private Strategy strategy;

	public StrategyCreateCommand() {
		super();
		gsnDiagram = null;
		strategy = null;
	}

	public void setStrategy(Object s) {
		if (s instanceof Strategy)
			this.strategy = (Strategy) s;
	}

	public void setGsnDiagram(Object e) {
		if (e instanceof GsnDiagram)
			this.gsnDiagram = (GsnDiagram) e;
	}

	public void setLayout(Rectangle r) {
		if (strategy == null)
			return;
		strategy.setLayout(r);
	}

	@Override
	public boolean canExecute() {
		if (strategy == null || gsnDiagram == null)
			return false;
		return true;
	}

	@Override
	public void execute() {
		gsnDiagram.addChild(strategy);
	}

	@Override
	public boolean canUndo() {
		if (gsnDiagram == null || strategy == null)
			return false;
		return gsnDiagram.contains(strategy);
	}

	@Override
	public void undo() {
		gsnDiagram.removeChild(strategy);
	}

}
