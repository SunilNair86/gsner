package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.gsn.model.business.Context;

public class ContextChangeLayoutCommand extends AbstractLayoutCommand {

	private Context model;
	private Rectangle layout;
	private Rectangle oldLayout;

	public void execute() {
		model.setLayout(layout);
	}

	public void setConstraint(Rectangle rect) {
		this.layout = rect;
	}

	public void setModel(Object model) {
		this.model = (Context) model;
		this.oldLayout = ((Context) model).getLayout();
	}

	public void undo() {
		this.model.setLayout(this.oldLayout);
	}

}
