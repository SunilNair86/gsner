package org.gsn.editor.command;

import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.gsn.model.business.Solution;

public class QuantifyConfidenceCommand extends Command {

	private Solution solution;

	@Override
	public void execute() {
		MessageDialog.openInformation(Display.getDefault().getActiveShell(), "GSN Information",
				"User will be prompted to answer a set of questions here.");
	}

	public void setSolution(final Solution model) {
		this.solution = model;
	}

}
