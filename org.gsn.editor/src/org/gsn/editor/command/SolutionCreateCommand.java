package org.gsn.editor.command;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.Solution;

public class SolutionCreateCommand extends Command {
	private GsnDiagram gsnDiagram;
	private Solution solution;

	public SolutionCreateCommand() {
		super();
		gsnDiagram = null;
		solution = null;
	}

	public void setSolution(Object s) {
		if (s instanceof Solution)
			this.solution = (Solution) s;
	}

	public void setGsnDiagram(Object e) {
		if (e instanceof GsnDiagram)
			this.gsnDiagram = (GsnDiagram) e;
	}

	public void setLayout(Rectangle r) {
		if (solution == null)
			return;
		solution.setLayout(r);
	}

	@Override
	public boolean canExecute() {
		if (solution == null || gsnDiagram == null)
			return false;
		return true;
	}

	@Override
	public void execute() {
		gsnDiagram.addChild(solution);
	}

	@Override
	public boolean canUndo() {
		if (gsnDiagram == null || solution == null)
			return false;
		return gsnDiagram.contains(solution);
	}

	@Override
	public void undo() {
		gsnDiagram.removeChild(solution);
	}

}
