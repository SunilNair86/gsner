package org.gsn.editor.editpart.tree;

import java.beans.PropertyChangeEvent;
import java.util.List;

import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.AbstractNode;

public class GsnDiagramTreeEditPart extends AppAbstractTreeEditPart {
	protected List<AbstractNode> getModelChildren() {
		return ((GsnDiagram) getModel()).getChildrenNodesArray();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ADD))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_REMOVE))
			refreshChildren();
	}

}
