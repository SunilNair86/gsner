package org.gsn.editor.editpart.tree;

import java.beans.PropertyChangeEvent;
import java.util.List;

import org.eclipse.gef.EditPolicy;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.gsn.editor.editpolicy.AppDeletePolicy;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Strategy;

public class StrategyTreeEditPart extends AppAbstractTreeEditPart {
	@Override
	protected List<AbstractNode> getModelChildren() {
		return ((Strategy) getModel()).getChildrenNodesArray();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new AppDeletePolicy());
	}

	@Override
	public void refreshVisuals() {
		final Strategy model = (Strategy) getModel();
		setWidgetText(model.getName());

		setWidgetImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT));
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ADD))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_REMOVE))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_RENAME))
			refreshVisuals();
		if (evt.getPropertyName().equals(Strategy.PROPERTY_COLOR))
			refreshVisuals();
	}
}
