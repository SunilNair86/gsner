package org.gsn.editor.editpart.tree;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.gsn.model.business.*;

public class AppTreeEditPartFactory implements EditPartFactory {
	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart part = null;

		if (model instanceof GsnDiagram)
			part = new GsnDiagramTreeEditPart();
		else if (model instanceof Goal)
			part = new GoalTreeEditPart();
		else if (model instanceof Solution)
			part = new SolutionTreeEditPart();
		else if (model instanceof Strategy)
			part = new StrategyTreeEditPart();
		else if (model instanceof Context)
			part = new ContextTreeEditPart();

		if (part != null)
			part.setModel(model);

		return part;
	}

}
