package org.gsn.editor.editpart.tree;

import java.beans.PropertyChangeEvent;
import java.util.List;

import org.eclipse.gef.EditPolicy;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.gsn.editor.editpolicy.AppDeletePolicy;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Solution;

public class SolutionTreeEditPart extends AppAbstractTreeEditPart {
	@Override
	protected List<AbstractNode> getModelChildren() {
		return ((Solution) getModel()).getChildrenNodesArray();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new AppDeletePolicy());
	}

	@Override
	public void refreshVisuals() {
		final Solution model = (Solution) getModel();
		setWidgetText(model.getName());

		setWidgetImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_DEF_VIEW));
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_LAYOUT))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ADD))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_REMOVE))
			refreshChildren();
	}
}
