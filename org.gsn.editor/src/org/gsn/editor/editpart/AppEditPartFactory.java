package org.gsn.editor.editpart;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.gsn.model.business.*;

public class AppEditPartFactory implements EditPartFactory {

	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		AbstractGraphicalEditPart part = null;

		if (model instanceof GsnDiagram) {
			part = new GsnDiagramPart();
		} else if (model instanceof Goal) {
			part = new GoalPart();
		} else if (model instanceof Solution) {
			part = new SolutionPart();
		} else if (model instanceof Strategy) {
			part = new StrategyPart();
		} else if (model instanceof Context) {
			part = new ContextPart();
		} else if (model instanceof Connection) {
			part = new ConnectionPart();
		}

		part.setModel(model);
		return part;
	}

}
