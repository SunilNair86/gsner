package org.gsn.editor.editpart;

import java.beans.PropertyChangeEvent;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.gsn.editor.editpolicy.AppConnectionPolicy;
import org.gsn.editor.editpolicy.AppDeletePolicy;
import org.gsn.editor.editpolicy.AppEditLayoutPolicy;
import org.gsn.editor.figure.ContextFigure;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Connection;
import org.gsn.model.business.Context;

@Data
@EqualsAndHashCode(callSuper = true)
public class ContextPart extends AppAbstractEditPart implements NodeEditPart {

	@Override
	protected IFigure createFigure() {
		final IFigure figure = new ContextFigure();
		return figure;
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new AppDeletePolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new AppEditLayoutPolicy());
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new AppConnectionPolicy());
	}

	@Override
	protected void refreshVisuals() {
		final ContextFigure figure = (ContextFigure) getFigure();
		final Context model = (Context) getModel();

		figure.setName(model.getName());
		figure.setDescription(model.getDescription());
		figure.setLayout(model.getLayout());
		figure.setBackgroundColor(model.getColor());
	}

	@Override
	public List<AbstractNode> getModelChildren() {
		return ((Context) getModel()).getChildrenNodesArray();
	}

	@Override
	public List<Connection> getModelSourceConnections() {
		return ((Context) getModel()).getSourceConnectionsArray();
	}

	@Override
	public List<Connection> getModelTargetConnections() {
		return ((Context) getModel()).getTargetConnectionsArray();
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_LAYOUT))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ADD))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_REMOVE))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_RENAME))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_COLOR))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.SOURCE_CONNECTION))
			refreshSourceConnections();
		if (evt.getPropertyName().equals(AbstractNode.TARGET_CONNECTION))
			refreshTargetConnections();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_DESCRIPTION))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ATTRIBUTES))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_IDENTIFIER))
			refreshVisuals();
	}
}
