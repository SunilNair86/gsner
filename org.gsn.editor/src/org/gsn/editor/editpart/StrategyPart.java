package org.gsn.editor.editpart;

import static com.google.common.collect.Lists.newArrayList;

import java.beans.PropertyChangeEvent;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.gsn.editor.editpolicy.AppConnectionPolicy;
import org.gsn.editor.editpolicy.AppDeletePolicy;
import org.gsn.editor.editpolicy.AppEditLayoutPolicy;
import org.gsn.editor.figure.StrategyFigure;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Connection;
import org.gsn.model.business.Strategy;

@Data
@EqualsAndHashCode(callSuper = true)
public class StrategyPart extends AppAbstractEditPart implements NodeEditPart {

	@Override
	protected IFigure createFigure() {
		return new StrategyFigure();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new AppDeletePolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new AppEditLayoutPolicy());
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new AppConnectionPolicy());
	}

	@Override
	protected void refreshVisuals() {
		final StrategyFigure figure = (StrategyFigure) getFigure();
		final Strategy model = (Strategy) getModel();

		figure.setName(model.getName());
		figure.setDescription(model.getDescription());
		figure.setLayout(model.getLayout());
		figure.setBackgroundColor(model.getColor());
	}

	@Override
	public List<AbstractNode> getModelChildren() {
		return newArrayList();
	}

	@Override
	public List<Connection> getModelSourceConnections() {
		return ((Strategy) getModel()).getSourceConnectionsArray();
	}

	@Override
	public List<Connection> getModelTargetConnections() {
		return ((Strategy) getModel()).getTargetConnectionsArray();
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_LAYOUT))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ADD))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_REMOVE))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_RENAME))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_COLOR))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.SOURCE_CONNECTION))
			refreshSourceConnections();
		if (evt.getPropertyName().equals(AbstractNode.TARGET_CONNECTION))
			refreshTargetConnections();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_DESCRIPTION))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ATTRIBUTES))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_IDENTIFIER))
			refreshVisuals();
	}
}
