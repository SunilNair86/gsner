package org.gsn.editor.editpart;

import java.beans.PropertyChangeEvent;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.gsn.editor.editpolicy.AppConnectionDeleteEditPolicy;
import org.gsn.editor.editpolicy.AppEditLayoutPolicy;
import org.gsn.model.business.AbstractNode;
import org.gsn.model.business.Connection;

public class ConnectionPart extends AppAbstractConnectionEditPart {

	private RectangleFigure acpRectangle;
	private final Label acpName = new Label();

	@Override
	protected IFigure createFigure() {
		final PolylineConnection connection = (PolylineConnection) super.createFigure();
		connection.setLineWidth(2);
		connection.setLineStyle(SWT.LINE_SOLID);
		final PolygonDecoration decoration = new PolygonDecoration();
		decoration.setTemplate(PolygonDecoration.TRIANGLE_TIP);
		decoration.setScale(15.0, 5.0);
		connection.setTargetDecoration(decoration);

		switch (((Connection) getModel()).getConnectionType()) {
		case 1:
			break;
		case 2:
			decoration.setBackgroundColor(ColorConstants.white);
			break;
		default:
			return null;
		}
		acpRectangle = new RectangleFigure();
		acpRectangle.setVisible(false);
		acpRectangle.setForegroundColor(ColorConstants.black);
		acpRectangle.setBackgroundColor(ColorConstants.black);
		acpRectangle.setFill(true);
		acpRectangle.setSize(20, 20);
		final MidpointLocator locator = new MidpointLocator(connection, 0);
		locator.setRelativePosition(PositionConstants.EAST);
		locator.setGap(20);
		connection.add(acpRectangle, new MidpointLocator(connection, 0));
		connection.add(acpName, locator);
		return connection;
	}

	@Override
	protected void refreshVisuals() {
		final Connection model = (Connection) getModel();
		acpRectangle.setVisible(model.getAcp() != null);
		acpName.setVisible(model.getAcp() != null);

		if (model.getAcp() != null)
			setAcpName(model.getAcp().getName());
	}

	public void setAcpName(final String name) {
		acpName.setText(name);
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.CONNECTION_ROLE, new AppConnectionDeleteEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE, new ConnectionEndpointEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new AppEditLayoutPolicy());
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(Connection.PROPERTY_ACP))
			refreshVisuals();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_RENAME))
			refreshVisuals();
		if (evt.getPropertyName().equals(Connection.PROPERTY_ACP_RENAME))
			refreshVisuals();
	}

	@Override
	public void performRequest(final Request req) {
		if (req.getType().equals(RequestConstants.REQ_OPEN)) {
			final Connection model = (Connection) getModel();
			if (model.getAcp() == null)
				return;
			final IEditorInput editorInput = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor().getEditorInput();
			final FileEditorInput fileInput = (FileEditorInput) editorInput;
			final IProject project = fileInput.getFile().getProject();
			final IFile file = project.getFile(StringUtils.join(model.getAcp().getName(), ".", "gsn"));
			if (!file.exists()) {
				final InputStream source = new ByteArrayInputStream("".getBytes());
				try {
					file.create(source, false, null);
				} catch (final CoreException e) {
					e.printStackTrace();
				}
			}
			try {
				IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), file);
			} catch (final PartInitException e) {
				e.printStackTrace();
			}
		}

	}
}
