package org.gsn.editor.editpart;

import java.beans.PropertyChangeListener;

import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.gsn.model.business.Connection;

public abstract class AppAbstractConnectionEditPart extends AbstractConnectionEditPart implements PropertyChangeListener {

	public void activate() {
		super.activate();
		((Connection) getModel()).addPropertyChangeListener(this);
	}

	public void deactivate() {
		super.deactivate();
		((Connection) getModel()).removePropertyChangeListener(this);
	}

	@Override
	protected void createEditPolicies() {}

}
