package org.gsn.editor.editpart;

import java.beans.PropertyChangeListener;

import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.gsn.model.business.AbstractNode;

public abstract class AppAbstractEditPart extends AbstractGraphicalEditPart implements PropertyChangeListener {
	@Override
	public void activate() {
		super.activate();
		((AbstractNode) getModel()).addPropertyChangeListener(this);
	}

	@Override
	public void deactivate() {
		super.deactivate();
		((AbstractNode) getModel()).removePropertyChangeListener(this);
	}

	@Override
	public void performRequest(final Request req) {
		if (req.getType().equals(RequestConstants.REQ_OPEN)) {
			try {
				final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				page.showView(IPageLayout.ID_PROP_SHEET);
			} catch (final PartInitException e) {
				e.printStackTrace();
			}
		}
	}
}
