package org.gsn.editor.editpart;

import java.beans.PropertyChangeEvent;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.LayerConstants;
import org.eclipse.swt.SWT;
import org.gsn.editor.editpolicy.AppEditLayoutPolicy;
import org.gsn.editor.figure.GsnDiagramFigure;
import org.gsn.model.business.GsnDiagram;
import org.gsn.model.business.AbstractNode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GsnDiagramPart extends AppAbstractEditPart {
	@Override
	protected IFigure createFigure() {
		IFigure figure = new GsnDiagramFigure();
		ConnectionLayer connLayer = (ConnectionLayer) getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setAntialias(SWT.ON);
		connLayer.setConnectionRouter(new ShortestPathConnectionRouter(figure));
		return figure;
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new AppEditLayoutPolicy());
	}

	public List<AbstractNode> getModelChildren() {
		return ((GsnDiagram) getModel()).getChildrenNodesArray();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_ADD))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_REMOVE))
			refreshChildren();
		if (evt.getPropertyName().equals(AbstractNode.PROPERTY_RENAME))
			refreshVisuals();
	}
}
