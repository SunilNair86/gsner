package org.gsn.editor.figure;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;

@Data
@EqualsAndHashCode(callSuper = true)
public class StrategyFigure extends Shape {
	public static final int STRATEGY_FIGURE_DEFWIDTH = 75;
	public static final int STRATEGY_FIGURE_DEFHEIGHT = 50;

	private Label labelName = new Label();
	private TextFlow textFlow = new TextFlow();
	private PointList points = new PointList();

	public StrategyFigure() {
		final GridLayout layout = new GridLayout();
		setLayoutManager(layout);

		labelName.setForegroundColor(ColorConstants.black);
		final Font classFont = new Font(null, "Arial", 10, SWT.BOLD);
		labelName.setFont(classFont);
		add(labelName, new GridData(GridData.CENTER, GridData.BEGINNING, false, false));

		final FlowPage flowPage = new FlowPage();
		textFlow.setLayoutManager(new ParagraphTextLayout(textFlow, ParagraphTextLayout.WORD_WRAP_TRUNCATE));
		flowPage.getLayoutManager().setConstraint(textFlow, new GridData(SWT.CENTER, SWT.CENTER, false, false));
		flowPage.setHorizontalAligment(PositionConstants.CENTER);
		flowPage.add(textFlow);
		add(flowPage, new GridData(GridData.CENTER, GridData.CENTER, true, true));

		setLineWidth(2);
		setOpaque(true);
	}

	@Override
	protected void fillShape(final Graphics graphics) {
		graphics.fillPolygon(getPoints(getBounds()));
	}

	@Override
	protected void outlineShape(final Graphics graphics) {
		graphics.drawPolygon(getPoints(getBounds()));
	}

	private PointList getPoints(final Rectangle r) {
		points.removeAllPoints();
		points.addPoint(r.getTopLeft().x + 13, r.getTopLeft().y);
		points.addPoint(r.getBottomLeft());
		points.addPoint(r.getBottomRight().x - 13, r.getBottomRight().y);
		points.addPoint(r.getTopRight());
		return points;
	}

	public void setName(final String text) {
		labelName.setText(text);
	}

	public void setDescription(final String description) {
		textFlow.setText(description);
	}

	public void setLayout(final Rectangle rect) {
		getParent().setConstraint(this, rect);
	}
}
