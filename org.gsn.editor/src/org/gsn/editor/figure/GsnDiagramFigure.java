package org.gsn.editor.figure;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

@Data
@EqualsAndHashCode(callSuper = true)
public class GsnDiagramFigure extends Figure {
	private XYLayout layout;

	public GsnDiagramFigure() {
		layout = new XYLayout();
		setLayoutManager(layout);
	}

	public void setLayout(Rectangle rect) {
		setBounds(rect);
	}

}
