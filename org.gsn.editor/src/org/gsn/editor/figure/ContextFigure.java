package org.gsn.editor.figure;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;

@Data
@EqualsAndHashCode(callSuper = true)
public class ContextFigure extends RoundedRectangle {
	public static final int CONTEXT_FIGURE_DEFWIDTH = 75;
	public static final int CONTEXT_FIGURE_DEFHEIGHT = 50;

	private Label labelName = new Label();
	private TextFlow textFlow = new TextFlow();

	public ContextFigure() {
		final GridLayout layout = new GridLayout();
		setLayoutManager(layout);

		labelName.setForegroundColor(ColorConstants.black);
		final Font classFont = new Font(null, "Arial", 10, SWT.BOLD);
		labelName.setFont(classFont);
		add(labelName, new GridData(GridData.CENTER, GridData.BEGINNING, false, false));

		final FlowPage flowPage = new FlowPage();
		textFlow.setLayoutManager(new ParagraphTextLayout(textFlow, ParagraphTextLayout.WORD_WRAP_TRUNCATE));
		flowPage.getLayoutManager().setConstraint(textFlow, new GridData(SWT.CENTER, SWT.CENTER, false, false));
		flowPage.setHorizontalAligment(PositionConstants.CENTER);
		flowPage.add(textFlow);
		add(flowPage, new GridData(GridData.CENTER, GridData.CENTER, true, true));

		setLineWidth(2);
		setOpaque(true);
		setCornerDimensions(new Dimension(30, 30));
	}

	public void setName(final String text) {
		labelName.setText(text);
	}

	public void setDescription(final String description) {
		textFlow.setText(description);
	}

	public void setLayout(final Rectangle rect) {
		getParent().setConstraint(this, rect);
	}
}
